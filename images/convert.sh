#!/bin/bash

case $1 in
    pressready)
	for f in `find pressready -name "*.png"`; do
	    name=${f#pressready/}
	    command="ln -sf $f $name"
	    echo $command
	    eval $command
	done
	;;
    resize)
	# 600dpi/72dpi=8.333...
	rm -fr tmp
	mkdir -p tmp
	for f in `find raw -name "*.png"`; do
	    name=${f#raw/}
	    command="convert raw/$name -resize 833% -type GrayScale tmp/$name"
	    echo $command
	    eval $command
	done
	;;
    verify)
	for f in `find pressready -name "*.png"`; do
	    command="identify -verbose $f | \grep -E 'Resolution|Units|Colorspace'"
	    echo $command
	    eval $command
	done
	;;
    *)
	# For web
	for f in `find raw -name "*.png"`; do
	    name=${f#raw/}
	    command="ln -sf raw/$name $name"
	    echo $command
	    eval $command
	done
	;;
esac
