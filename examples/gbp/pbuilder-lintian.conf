# See https://wiki.debian.org/PackagingWithGit#Packaging_with_Git

[DEFAULT]
builder = BUILDER=pbuilder git-pbuilder
cleaner = fakeroot debian/rules clean
# Create pristine-tar on import
pristine-tar = True
# Run lintian to check package after build
postbuild = docker run --rm -v $GBP_BUILD_DIR/..:/debian \
  debian:lintian \
  /debian/$(basename $GBP_CHANGES_FILE) 2>&1 \
  | tee ../lintian-full.log && \grep -E "^(E:|I:|W:|P:)" ../lintian-full.log \
  | tee ../lintian-simple.log && echo "Lintian DONE"
debian-branch = debian/unstable

[buildpackage]
export-dir = ../build-area/

[import-orig]
# Filter out unwanted files/dirs from upstream
filter = [
    '*egg.info',
    '.bzr',
    '.hg',
    '.hgtags',
    '.svn',
    'CVS',
    '*/debian/*',
    'debian/*'
    ]
# filter the files out of the tarball passed to pristine-tar
filter-pristine-tar = True
pristine-tar = True

[import-dsc]
filter = [
    'CVS',
    '.cvsignore',
    '.hg',
    '.hgignore',
    '.bzr',
    '.bzrignore',
    '.gitignore'
    ]

[dch]
# ignore merge commit messages
git-log = --no-merges
