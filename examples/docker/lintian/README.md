# lintianをdockerコンテナ内で実行するためのサンプル

## 使い方

最初にlintianをインストール済みのイメージをビルドする。

```
docker build -t debian:lintian .
```

ホストでソースパッケージをビルドする。

```
$ apt source hello
$ (cd hello-2.10 && debuld -S -us -uc)
```

.changesができるので、それに対してlintianを実行する。

```
$ docker run --rm -v $PWD:/debian debian:lintian /debian/hello_2.10-2_source.changes
```
