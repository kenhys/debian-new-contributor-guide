#!/bin/bash

set -xue

if [ -f "$1" ]; then
    PDF=$1
else
    PDF=$(dirname $0)/../Debian新コントリビューターガイド.pdf
fi
mkdir -p tmp
for p in 1 2 4 5 6 7 8 9 130 131 132 133; do
    pdftk $PDF cat $p output tmp/page$p.pdf
    pdftocairo -png -r 96 -singlefile tmp/page$p.pdf page$p
    mediainfo tmp/page$p.png
done
find tmp
