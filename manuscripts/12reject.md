# パッケージがRejectされてもあきらめない

前章では、RFSをだしてスポンサーを探す手順を紹介しました。
本章では、rejectされた場合の対応について説明します。

## パッケージがrejectされるとき

スポンサーにパッケージをアップロードしてもらって、しばらくたち、忘れていたころにrejectの通知はやってきます。


例えば、libfreeaptxをアップロードしてしばらくして、次のような件名のメールが `<ftpmaster@ftp-master.debian.org>`から届きました。

```
Subject: libfreeaptx_0.1.1-1_amd64.changes REJECTED
```

ちなみに、rejectされた理由は*debian/copyright*の記載漏れによるものでした。

<section class="column">
<h4>Debianパッケージの審査にどれくらいかかるのか</h4>

決まった期間というのはありません、パッケージの審査を担当するFTP masterチームのリソースに依存するからです。
なお、Debianの新しいバージョンがリリースされる前の時期は、フリーズされてしまうので、新しいパッケージをDebian公式アーカイブに入れるのには"時期が悪い"です。
</section>

## どのような基準でパッケージはrejectされるのか

典型的な例は、*debian/copyright*の記述が不十分な場合です。

rejectされる理由については、[Reject FAQ for Debian's NEW-Queue](https://ftp-master.debian.org/REJECT-FAQ.html)<span class="footnote">https://ftp-master.debian.org/REJECT-FAQ.html</span>にまとまっています。

それによると、Debian公式アーカイブに入れるときの優先度は次のようになっています。

* Debian公式アーカイブを法的に問題ない状態へ保つこと
* パッケージ名を適切なものにすること
* バグを減らすこと

*debian/copyright*の記述が不十分だと、ライセンスがきちんと確認されていないものを含むことになります。
これは、上記の方針に合致しないのでrejectされるわけです。

ほかにも、`lintian`によってチェックされる特定のタグに合致する場合にもrejectされます。
この対象となるタグは[lintian.tags](https://ftp-master.debian.org/static/lintian.tags)<span class="footnote">https://ftp-master.debian.org/static/lintian.tags</span>として定義されています。
このファイルで`fatal`とされているものにひっかかっていると一発でアウトです。

```text:rejectされるタグの一覧(抜粋)
# Overriding these tags is NOT allowed
fatal:
  - FSSTND-dir-in-usr
  - FSSTND-dir-in-var
  - bad-package-name
  ...
```

## どのように対処すべきか

rejectされた理由にもよりますが、Debianではどうやってもクリアできないライセンス上の問題等がないのであれば、
パッケージング側で適切な対処をしてあげる必要があります。

典型的なのは次のような場合です。

| 項目 | 対処方法 |
|---|---|
|*debian/copyright*の記述が不十分だった|`licensecheck`などのツールを使ってライセンスを再度チェックしなおす|
|Debianでは配布できないコードがある|問題のあるものを除く対処で実用上問題ないのなら、該当コードを除去して+dfsgバージョンをつけてパッケージングする|
|ポリシー違反|`lintian`が指摘するエラーをすべて修正する|

最初にスポンサーがアップロードするときに、ある程度チェックしてくれているはずですが、
やはりスポンサーも人なので、見落とすこともあります。

問題が修正できたら、再びスポンサーにアップロードしてもらいましょう。

もし、アップストリームが新しいバージョンをだしているなら、この機会に新しいバージョンをパッケージングしなおすとよいでしょう。
パッケージングしなおすときの手順は、第13章のパッケージの更新の手順の説明が参考になるでしょう。

## 本章のまとめ

本章では、パッケージがrejectされたときの対処について説明しました。

<section class="summary">

* ftp-masterのreject理由を完全に理解する
* もし新しいバージョンがでているなら今度は新しいバージョンでパッケージングしなおそう
* パッケージを修正したら再度スポンサーを確保しよう

</section>

次章では、パッケージがDebian公式アーカイブに入ったあとの更新の方法について説明します。

---

<section class="column">
<h4>Debianパッケージのバージョンについている+dfsgや+dsって何？</h4>

それぞれ、次のような意味があります。

* +dsfgはソースコードそのままだと、再配布できない問題があり、該当するものを削除したことを示します
* +dsはソースコードに再配布できない問題はなかったが、パッケージでインストールできるライブラリーなどをバンドルしていたため削除したことを示します
</section>

