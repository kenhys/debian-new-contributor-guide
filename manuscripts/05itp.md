# パッケージングする宣言(ITP)してみよう

ITPとはIntent to Packageの略で、パッケージングすることを宣言するべく行います。
これは他の人とパッケージング作業が被らないようにするためのものです。<span class="footnote">https://www.debian.org/devel/wnpp/being_packaged をみると現在パッケージング作業が進行しているものがわかるようになっています。</span>


ITPは特定のフォーマットで記述し、submit@bugs.debian.org宛にメールを送る必要があります。
これはDebianプロジェクトで採用されているバグトラッキングシステムが、メールの本文からバグ登録に必要なメタ情報を抽出する仕組みになっているからです。

この特定のフォーマットで記述するのを楽にしてくれるのが、`reportbug`というプログラムです。

reportbugでITPするには次のようにします。

* reportbugをインストールする(初回のみ)
* reportbugの初期設定を行う(初回のみ)
* reportbugでITPする

## reportbugをインストールする

次のコマンドでインストールできます。

```
$ sudo apt install -y reportbug-gtk python3-gtkspellcheck
```

スペルチェックを有効にするには、`python3-gtkspllcheck`もあわせてインストールするのをおすすめします。
バグ報告の雛形をGUIで作成できるので、`reportbug-gtk`を推奨しています。<span class="footnote">ターミナルの画面で操作を完結させたいなら`reportbug`のみのインストールで構いません。</span>

## reportbugの初期設定を行う

`reportbug`には初期設定が必要です。
`Sendmail`や`Postfix`といったMTA<span class="footnote">Mail Transfer Agentの略。Sendmail、Postfixなどが有名。</span>の設定がされておらず、シンプルなGTKのインターフェースを使う場合の設定は次のとおりです。`~/.reportbugrc`を作成し、下記のような内容を反映しておきましょう。

<!-- TODO: 初期設定の内容を再度確認する -->

```text:~/.reportbugrc
mode novice
ui gtk
# あなたの氏名を記述する
realname "Debian User"
# あなたのメールアドレスを記述する
email "debian@example.jp"
no-cc
list-cc-me
smtphost reportbug.debian.org
```

## reportbugでITPする

ITPの雛形を作成するには、次のコマンドを実行して、`reportbug`を起動します。

```
$ reportbug wnpp
```

ウィザード形式の質問に答えていくと、ITPの雛形を作成できるようになっています。

おおむね次の手順で雛形が完成します。

* リクエストの種類を指定
* 対象パッケージを指定
* 件名を指定
* 既存のITPを検索
* パッケージの説明を記述
* バグ報告の雛形を保存

では、どのようにITPすればいいのかを説明します。
題材には `libfreeaptx` <span class="footnote">https://github.com/iamthehorker/libfreeaptx</span>を使用します。

![reportbugを起動した直後の画面](../images/reportbug-itp-0-wnpp.png){width=70%}

起動直後の画面では、`reportbug`の説明が表示されます。

![リクエストの種類を選択する画面](../images/reportbug-itp-1-itp.png){width=70%}

リクエストの種類を選択する画面では、今回はパッケージングすることを宣言するので、ITPを選択します。

ITP以外の略語に関しては、[略語の解説](https://www.debian.or.jp/community/devel/abbreviation.html) <span class="footnote">https://www.debian.or.jp/community/devel/abbreviation.html</span> を参照するとよいでしょう。

![対象パッケージを指定する画面](../images/reportbug-itp-2-package-name.png){width=70%}

対象パッケージを指定する画面では、パッケージングしようとしてるもののパッケージ名を指定します。
今回は入力欄に「`libfreeaptx`」と入力し次に進みます。

![件名を指定する画面](../images/reportbug-itp-3-subject.png){width=70%}

件名を指定する画面では、ITPのときの件名を指定します。
パッケージの簡潔な説明文を入力する必要があるので、「Free Implementation of aptX」と入力し次に進みます。

![既存のITPを検索する画面](../images/reportbug-itp-4-not-match.png){width=70%}

既存のITPを検索する画面では、似たようなパッケージ名ですでにITPがなされているものを一覧で表示してくれます。
ITPの対象となるソフトウェアがこの一覧にないことを確認して、そのまま次に進みます。

![パッケージの説明を記述する画面](../images/reportbug-itp-5-description.png){width=70%}

パッケージの説明を記述する画面では、あらかじめテンプレートを埋めてくれているので、必要事項を更新します。

* Package name: パッケージ名を指定します
* Version: バージョンを指定します
* Upstream Author: 開発元の著者を指定します
* URL: ソフトウェアの入手先を指定します
* License: ソフトウェアのライセンスを指定します
* Programming Language: ソフトウェアを実装している言語を指定します
* Description: ソフトウェアの説明を記載します

Descriptionは短い説明と、詳細説明の両方を書きます。
なぜDebianパッケージ化するのか、そのソフトウェアの特徴などを明記します。
すでにDebianパッケージとして利用可能な似たような別のソフトウェアが存在する場合には、
類似ソフトウェアがあるなかで新たにパッケージングする理由を書くとよいでしょう。

![バグ報告の保存指定画面](../images/reportbug-itp-6-save.png){width=70%}

バグ報告の保存指定画面では、バグレポートを編集しなおすか、保存するかを選択できます。
とくに修正の必要がなければこのまま保存しましょう。


![バグ報告の保存先画面](../images/reportbug-itp-7-tmpfile.png){width=70%}

バグ報告の保存先画面では、保存を選択すると、一時ファイルとして*/tmp*配下に保存できます。

これでITPの雛形の作成は完了です。

![バグ報告の雛形作成完了画面](../images/reportbug-itp-8-finish.png){width=70%}

雛形の保存が終了したら、`reportbug`を閉じて、保存されたテンプレートを確認しましょう。

```text:/tmpに保存されたITPの雛形
Content-Type: text/plain; charset="us-ascii"
MIME-Version: 1.0
Content-Transfer-Encoding: 7bit
From: Kentaro Hayashi <kenhys@xdump.org>
To: Debian Bug Tracking System <submit@bugs.debian.org>
Subject: ITP: libfreeaptx -- Free implementation of aptX

Package: wnpp
Severity: wishlist
Owner: Kentaro Hayashi <kenhys@xdump.org>
X-Debbugs-Cc: debian-devel@lists.debian.org, kenhys@xdump.org

* Package name    : libfreeaptx
  Version         : 0.1.1
  Upstream Author : Hunter Wardlaw <wardlawhunter@gmail.com>
* URL             : https://github.com/iamthehorker/libfreeaptx
* License         : LGPL-2.1+
  Programming Lang: C
  Description     : Free implementation of Audio Processing Technology codec (aptX)

libfreeaptx is based on version 0.2.0 of libopenaptx with the intent of continuing
under a free license without the additional license restriction added to libopenaptx
0.2.1.
The initial version of libfreeaptx was reset to 0.1.0 to prevent confusion between
 the two projects.

NOTE:
* This library is required for pipewire to enable aptX
  support.
* libopenaptx had been already orphaned on Debian.
```

`libfreeapt`のパッケージ化をすすめたときは、`libopenaptx`が制約をともなうライセンスになってしまったことから、
自由なライセンスで配布できるものとして選択しました。

雛形の先頭部分はヘッダなので、`Package: wnpp`以下のテキストをメールの本文として
`submit@bugs.debian.org`宛にメールを送信します。

しばらくすると、次のような自動応答メールが送られてきます。

```
From: "Debian Bug Tracking System" <owner@bugs.debian.org>
To: Kentaro Hayashi <kenhys@xdump.org>
Reply-To: 995607@bugs.debian.org
Subject: Bug#995607: Acknowledgement (ITP: libfreeaptx -- Free implementation of \
Audio Processing Technology codec (aptX))
Date: Sun, 03 Oct 2021 06:54:04 +0000
X-Mailer: MIME-tools 5.509 (Entity 5.509)
X-GND-Status: LEGIT

Thank you for filing a new Bug report with Debian.

You can follow progress on this Bug here: 995607: 
https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=995607.
...
```

このメールがきた時点で、送信したITPのメールをもとにバグ登録が完了しています。

![https://bugs.debian.org/995607の画面](../images/bug995607.png){width=80%}

[Bug#995607](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=995607)<span class="footnote">https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=995607</span>にアクセスするとバグとしてきちんと登録されていることがわかります。

ITPの登録ができたので、作業が他の人と重複する心配がなくなりました。
次は実際に開発環境を整えて、パッケージの体裁を整えてみましょう。

---

## 本章のまとめ

本章では、`reportbug`を使ったITPのやりかたについて説明しました。

`reportbug`は、MTAが設定済みの環境を想定しているツールです。
`Thunderbird`などを普段メーラーとして使っているとだいぶ面食らうことでしょう。
<section class="summary">

* ITPは`reportbug`を使うと雛形を簡単に作成できる
* ITPはパッケージングにとりかかるまえに行うべし

</summary>


次の章では、パッケージング環境の構築方法について説明します。

---

<section class="column">

<h4>すでに誰か作業しているかを確認するにはどうすればいい?</h4>

reportbugで毎回確認するのは、ちょっと手間ですよね。
というわけで、作業が望まれるページ<span class="footnote">https://www.debian.org/devel/wnpp/</span>というのがまとめられています。

このページの以下の2つを確認するとよいでしょう。

* 現在作業中のパッケージ
* 要求があるパッケージ

「現在作業中のパッケージ」<span class="footnote">https://www.debian.org/devel/wnpp/being_packaged</span>というのはまさにすでにITPがなされていて、誰かが作業しているもののリストです。
「要求があるパッケージ」<span class="footnote">https://www.debian.org/devel/wnpp/requested</span>というのはパッケージ化してほしいというリクエストがあがっているもののリストです。

「現在作業中のパッケージ」にないことをあらかじめ確認しておくことをおすすめします。

![現在作業中のパッケージ画面](../images/wnpp-being-packaged.png){width=80%}

</section>

---

<br>

---

---
