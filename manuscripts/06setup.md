# パッケージング環境を構築してみよう

ITPのバグ登録ができたので、次はパッケージング作業をするための環境を構築してみましょう。

パッケージング作業をするにあたっては、いくつか選択肢があります。

* ホストに直接開発関連のパッケージをインストールする
* ホストとは別の環境(コンテナや仮想マシン等)を用意する

本章では、ホストに開発関連のパッケージをインストールしてパッケージング環境を構築する手順を紹介します。

普段使っている環境にできるだけあれこれインストールしたくないという場合もあるでしょう。
別の章にてコンテナをベースとした「whalebuilder」や「debocker」を使う場合についても扱います。

## 前提となる作業環境

Debianコントリビューターとして活動してみたいという人が、必ずしも開発版であるDebian sid<span class="footnote">sidのことをunstableと呼んだりもします。unstableといいますが、致命的な不具合を踏むことは(ここ最近)あまりありません。筆者もunstableを常用しています。</span>を使っているとは限りません。

そこで、現在の安定版であるDebian 12 (bookworm)<span class="footnote">本書を執筆時点では、2023年7月にリリースされた12.1が最新です。</span>上にDebian sid (unstable)向けのパッケージングを開発できるようにします。

パッケージング環境といっても、さまざまなツールが開発されており、それらの組み合わせはいろいろあります。
ある程度広く使われている、`git-buildpackage`を使ったワークフローで作業できるように、パッケージング環境を構築します。

`git-buildpackage`はパッケージのソースがGitで管理されているパッケージをメンテナンスするのに便利なツールです。

![パッケージで採用されているバージョン管理システムの変化](../images/vcs_testing-stacked.png){width=80%}

パッケージで採用されているバージョン管理システムの変化は [Debian Trends](https://trends.debian.net/) <span class="footnote">https://trends.debian.net/ Version Control Systemより引用。ほかにも各種統計情報が公開されている。</span>が出典です。

パッケージのリポジトリは、グラフからもわかるように圧倒的にGitが使われています。
`git-buildpackage`を覚えておいて損はないでしょう。

## 開発関連のパッケージをインストールする

パッケージング環境の構築では次の手順を順番に実施してください。

* `git-buildpackage`をインストールする
* `piuparts`をインストールする
* 既定のエディターを設定する
* パッケージの署名に使うGPG鍵を生成する
* *~/.bashrc*に環境変数を設定する

<!-- * *~/.devscripts*を設定する -->

* *~/.gbp.conf*を設定する
* `pbuilder`をセットアップする

### git-buildpackageをインストールする

Gitをベースとしたパッケージングをするのが昨今の主流なので、`git-buildpackage`とその関連パッケージを次のコマンドでインストールします。

```
$ sudo apt install --no-install-recommends -y git-buildpackage pristine-tar pbuilder
```

<!-- packaging-devはorphanされたので使わない。上記くみあわせだと243 => 43に減らせる -->

### piupartsをインストールする

ビルドしたパッケージのテストに使うので`piuparts`を次のコマンドでインストールします。

```
$ sudo apt install --no-install-recommends -y piuparts
```

<!-- --no-install-recommendsにより 12 => 7に減少 -->

<!--

### lintianを更新する

パッケージのチェックに使うツールが`lintian`です。

執筆時点のbookwormではDebian sid(不安定版)やtrixie(テスト版)と`lintian`のバージョンは同じですが、
将来的には、バージョンが古く見劣りする状態になることが考えられます。

bullseyeではlintianが古かったが、bookwormではsidと同じバージョンがリリースされているので更新の必要がない

もしbookwormの`lintian`が古い場合には、bookworm-backports<span class="footnote">bookworm向けに新しいバージョンを提供するリポジトリ。bookwormにはバグ修正等しかはいらないが、backportsには新しいパッケージを入れられるので、Debianのリリースサイクルと合わないソフトウェアなどはbackportsで提供される。</span>のバージョンを使います。

既定では、bookwormのパッケージのみを取得するようになっています。
そこで、bookwormでも新しいバージョンを使えるように、bookworm-backportsからパッケージをとってこれるように*/etc/apt/sources.list.d/backports.list*を追加します。

```conf:/etc/apt/sources.list.d/backports.listの内容
deb http://deb.debian.org/debian/ bookworm-backports main
deb-src http://deb.debian.org/debian/ bookworm-backports main
```

次のコマンドを実行して、`lintian`をインストールします。

```
$ sudo apt update
$ sudo apt install -y -t bookworm-backports lintian
```

<section class="column">

<h4>なぜlintianを更新したほうがよいのか</h4>

`lintian`はパッケージをチェックするというソフトウェアの性質上、新しいポリシーに対応するなど、頻繁に更新されています。

そのため、安定版であるDebian bookwormに含まれるバージョンは古く、新しいポリシーやルールに対応していない、ということが起きます。

後々の話にはなりますが、パッケージの審査待ちの状態になってから、実際に審査されるまでには時間がかかります。
古いバージョンの`lintian`で見落とされていたエラーのせいで(実際に審査がはじまった段階で)審査落ちするという事態はうれしくありません。それゆえ、`lintian`は新しいバージョンを使うべきなのです。

</section>

最新の`lintian`を使うという意味では、`unstable`なり、`testing`のものをインストールするという方法もありますが、bookworm向けにパッケージングされているものがあるので、bookworm-backportsを使う手順を紹介しています。
なお、別の章で`lintian`も`docker`イメージを使って最新版で実行する方法を説明しています。

-->

### dh-makeをインストールする

Debianパッケージの雛形を生成するために、`dh-make`をインストールします。

```
$ sudo apt install --no-install-recommends -y dh-make
```

<!-- 40 => 34 --> 

### 既定のエディターを設定する

既定のエディターはあらかじめ好みのものに変更しておくとよいでしょう。
変更するためには、`update-alternatives`でエディタの優先度を変更します。

```
$ sudo update-alternatives --config editor
There are 3 choices for the alternative editor (providing /usr/bin/editor).

  Selection    Path                Priority   Status
------------------------------------------------------------
  0            /bin/nano            40        auto mode
  1            /bin/nano            40        manual mode
* 2            /usr/bin/vim.basic   30        manual mode
  3            /usr/bin/vim.tiny    15        manual mode

Press <enter> to keep the current choice[*], or type selection number: 2
```

変更しておかないと、ツールによっては既定で`nano`が自動的に起動して面倒臭いことになります。<span class="footnote">キーバインディングがWindowsのアプリケーション風なので、Windowsになれている人は使いやすいのでしょう。</span>

### パッケージの署名に使うGPG鍵を生成する

パッケージのメンテナンスには、GPGでパッケージに署名する機会があります。
もし、まだ作成していないようなら、新しい GPG キーを生成する<span class="footnote">https://docs.github.com/ja/authentication/managing-commit-signature-verification/generating-a-new-gpg-key</span>などを参照して、GPG鍵を作成しましょう。

普段使っているGPG鍵があるならそれをそのまま使ってかまいません。

### ~/.bashrcに環境変数を設定する

ログインシェルに`bash`を利用しているなら、*~/.bashrc*に次のように`DEBEMAIL`と`DEBFULLNAME`を設定します。
ほかのログインシェルをお使いのかたは適宜読み替えてください。

```
# あなたのメールアドレスを指定
export DEBEMAIL=debian@example.jp
# あなたの氏名を指定
export DEBFULLNAME="Debian User"
```

ここで設定した値は`dch`<span class="footnote">debianパッケージのchangelogの更新に使うプログラムです。</span>などで使われます。

<!--

### ~/.devscriptsを設定する

*~/.devscripts*は`devscripts`パッケージに含まれるツールのための設定ファイルです。

*~/.devscritps*に次のように`DEBSIGN_KEYID`を設定します。

DEBSIGN_KEYIDに指定する値は、GPG鍵のfingerprintを指定します。

```text:~/.devscripts
DEBSIGN_KEYID=89105F274BCFCAA629CBCFA3317478E3CABB9E20
```

ここで設定した値は`debuild`<span class="footnote">debファイルをビルドするのに使うプログラム。</span>でパッケージの署名まで行うときなどに使われます。

/-->

### pbuilderをセットアップする

クリーンな環境でパッケージを1からビルドするためのツールが`pbuilder`です。
普段使っている環境でだけビルドすると、依存関係がきちんと記述されていないことに気づけません。
そのため、unstableのイメージを用意して、そのなかでパッケージをビルドするようにします。

ビルドディレクトリをtmpfsでマウントするとディスクIOを減らせるので`pbuilder`の実行を高速化できます。
`pbuilder`でのビルドのために、設定ファイルに`APTCACHEHARDLINK=no`を追加します。次のコマンドを実行します。

```
$ echo "APTCACHEHARDLINK=no" | sudo tee -a /etc/pbuilderrc 
```

*/etc/fstab*に次のようなエントリを追加して、*/var/cache/pbuilder/build*を`tmpfs`としてマウントします。

```cmd:/etc/fstabでtmpfsとしてマウントするための設定
tmpfs   /var/cache/pbuilder/build tmpfs   defaults        0       0
```

また、ビルド中に実行するフックを設定します。次のコマンドを実行してフックの配置場所を指定します。

```
$ echo "HOOKDIR=/usr/lib/pbuilder/hooks" | sudo tee -a /etc/pbuilderrc
```

そして、*/usr/lib/pbuilder/hooks*にビルド後に実行されるフックスクリプトをコピーします。

自動テストとパケージ内容のチェックを有効にするために、次のコマンドを実行します。

```
$ sudo mkdir -p /usr/lib/pbuilder/hooks
$ sudo cp /usr/share/doc/pbuilder/examples/B20autopkgtest /usr/lib/pbuilder/hooks
$ sudo cp /usr/share/doc/pbuilder/examples/B90lintian /usr/lib/pbuilder/hooks
```

<!--
autopkgtestはホストにインストールしなくてもいい 
B90list-missingはfakerootがrootfsにないので失敗する?
 -->

`pbuilder`でunstableのイメージを構築するには次のコマンドを実行します。

```
$ sudo pbuilder create
```

上記を実行すると、*/var/cache/pbuilder/base.tgz*にunstableのイメージが作成されます。

<section class="column">

<h4>pbuilderで作成したunstableのイメージを更新するには</h4>

`pbuilder`で作成したunstableのベースイメージは、定期的に更新する必要があります。
これは、unstableのパッケージが頻繁に更新されるためです。

古いバージョンのベースイメージ(/var/cache/pbuilder/base.tgz)でパッケージをビルドできていても、最新のunstableの環境ではビルドできないということがありえます。
パッケージをビルドしている途中で依存パッケージのインストールに失敗しているようなら、ベースイメージが古い可能性を疑うべきです。

ベースイメージを更新するには次のコマンドを実行します。

```
$ sudo pbuilder update
```

</section>

`pbuilder`のセットアップがうまくいっているか、実際にビルドして確認してみましょう。
サンプルとしてhelloパッケージをビルドするには次のコマンドを実行します。

```
$ apt source hello
$ sudo pbuilder --build ./hello_2.10-3.dsc
```

helloパッケージのソースを取得して、`pbuilder`を使ってクリーンなunstableの環境でビルドできます。

```cmd:helloパッケージをビルドしているときのログ
...
dpkg-deb: building package 'hello' in '../hello_2.10-3_amd64.deb'.
dpkg-deb: building package 'hello-dbgsym' in '../hello-dbgsym_2.10-3_amd64.deb'.
 dpkg-genbuildinfo -O../hello_2.10-3_amd64.buildinfo
 dpkg-genchanges -O../hello_2.10-3_amd64.changes
dpkg-genchanges: info: not including original source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: binary and diff upload (original source NOT included)
dpkg-genchanges: info: not including original source code in upload
...
```

パッケージのビルドが成功すると、*/var/cache/pbuilder/result*以下にdebが作成されます。

```cmd:
$ tree /var/cache/pbuilder/result/
/var/cache/pbuilder/result/
├── hello-dbgsym_2.10-3_amd64.deb
├── hello_2.10-3.debian.tar.xz
├── hello_2.10-3.dsc
├── hello_2.10-3_amd64.buildinfo
├── hello_2.10-3_amd64.changes
├── hello_2.10-3_amd64.deb
└── hello_2.10-3_source.changes

1 directory, 7 files
```

### ~/.gbp.confを設定する

*~/.gbp.conf*は`gbp`コマンドの設定ファイルです。`git-buildpackage`パッケージに含まれています。

`gbp`はGitでパッケージのソースコードを管理している場合に便利なツールです。
パッケージのビルドだけでなく、パッチの管理など、Gitと親和性が高いです。
初期設定をしておきましょう。

下記は[Packaging with Git](https://wiki.debian.org/PackagingWithGit#Packaging_with_Git)<span class="footnote">https://wiki.debian.org/PackagingWithGit#Packaging_with_Git</span>をベースにしたものです。

* debパッケージのビルドには`pbuilder`を使う
* パッケージをリポジトリにインポートするときのフィルタを追加
* 既定のdebianパッケージ向けブランチを`debian/unstable`に変更

上記の3つのカスタマイズをしています。

```text:~/.gbp.confの内容
[DEFAULT]
builder = BUILDER=pbuilder git-pbuilder
cleaner = fakeroot debian/rules clean
pristine-tar = True
debian-branch = debian/unstable
[buildpackage]
export-dir = ../build-area/
ignore-new = True
[import-orig]
filter = ['*egg.info', '.bzr', '.hg', '.hgtags', '.svn', 'CVS', \
  '*/debian/*', 'debian/*']
filter-pristine-tar = True
pristine-tar = True
[import-dsc]
filter = ['CVS', '.cvsignore', '.hg', '.hgignore', '.bzr', '.bzrignore', '.gitignore']
[dch]
git-log = --no-merges
```

設定ファイルのサンプルは本書のリポジトリから入手 <span class="footnote">https://salsa.debian.org/kenhys/debian-new-contributor-guide/-/blob/master/examples/gbp/pbuilder.conf</span>できます。

<section class="column">

<h4>ホストに開発関連のパッケージを極力インストールせず使うには</h4>

ホストに極力開発関連のパッケージはインストールしたくない場合には、コンテナベースの環境を構築するとよいでしょう。

極力パッケージをインストールしないためには、次の手が使えます。

* `podman`を導入し、パッケージのビルド等の試行錯誤をコンテナ環境で完結させる
* パッケージの体裁が整ったら、コンテナ環境でソースパッケージ(.dsc)を作成する。クリーンビルドは`pbuilder`だったり`debocker`などを使って行う

上記は、Debian unstableでパッケージがソースからビルドできないFTBFSとよばれる問題に遭遇したときなどによく筆者が好んで使う手です。

</section>

## 本章のまとめ

本章では、Debian 12上にDebian sid (unstable)のパッケージをビルドする環境の構築方法を説明しました。

<section class="summary">

* Debian公式アーカイブに入れるには、unstable向けにパッケージを用意しないといけない
* `gbp`を使うのがおすすめ。最初は`pbuilder`でクリーンなビルド環境を用意するのがよい
* 慣れてきたら`gbp buildpackage`のバックエンドを`docker`ベースに切り替えてもよいかも

</section>

次の章では、この環境を使って実際にパッケージングのやりかたを説明していきます。

---
---
