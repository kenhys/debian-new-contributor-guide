# Debianパッケージングの流れを知ろう

そもそも、Debianプロジェクトでパッケージをリリースできるようにするにはどうすればよいでしょうか。

それを簡単に説明している文書の1つに、Introduction for maintainers: How will my package get into Debian<span class="footnote">https://mentors.debian.net/intro-maintainers/</span>があります。これは、mentors.debian.netというパッケージを一時的にアップロードするためのサイトで公開されているドキュメントです。<span class="footnote">mentors.debian.netのコンテンツは日本語への翻訳がすすめられています。</span>

簡単に紹介すると次の通りです。

* パッケージング対象のソフトウェアを決める
* パッケージングする宣言をバグとして登録する
* 対象ソフトウェアのパッケージを用意する
* パッケージをレビューしてもらうために公開する
* スポンサーを見つけ、レビューを受ける
* スポンサーにパッケージをアップロードしてもらう
* 審査を受けて問題なければDebian公式のアーカイブに入る

本書を読んでいるということは、なんらかのパッケージをDebianに入れたいということでしょうから、実質「パッケージングする宣言をバグとして登録する」ところからはじめることになります。


その後、パッケージの体裁を整え、スポンサーしてもらってDebianアーカイブに入れてもらうというのがおおまかな流れです。

---

## 本章のまとめ

本章では、Debianパッケージングの流れを説明しました。
Debianコントリビューターとしてやるべきことは、

<section class="summary">

* まずはITPを実施して作業がかぶらないようにしよう
* レビューしてもらえる状態のパッケージを作成しよう
* スポンサーを見つけてレビュー&アップロードしてもらおう

</section>

ということになります。

次の章では、実際のパッケージング例を紹介しながら、Debianパッケージングの基本をおさえていきます。まずはパッケージングする宣言をどうやったらいいのかを説明します。

<section class="column">

<h4>Debian向けにパッケージングするとなぜ嬉しいのか</h4>

Debianパッケージは、ある時期がくるとUbuntuにとりこまれます。
UbuntuにはPPAと呼ばれる自分でパッケージを公開する仕組みが整っていますが、それはあくまで非公式なパッケージであり、標準でインストールできるようにはなっていません。

Debian公式アーカイブに入れると、その成果はとくになにもしなくてもUbuntu(とその派生ディストリビューション)に反映されるというのがメリットです。また、ライセンス上の問題をクリアして配布されており、パッケージングポリシーに準拠しているなど、ある程度の品質が保たれる仕組みになっています。安心して使えるというのもメリットといえるでしょう。

場合によってはソフトウェアの開発元がdebパッケージをリリースのたびに公開してくれていることがありますが、ライセンスファイルが含まれていなかったり、
システムにインストールされているライブラリーを使わず、独自に同梱しているものを使うようになっていたりするなど使い勝手が異なることがあります。
</section>

