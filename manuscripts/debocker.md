---
class: practice
---

# 実践: debockerを使ってビルドする方法

`docker`ベースでパッケージをビルドするツールの1つに`debocker`があります。
bookwormでは0.2.5が最新です。
ホームページ<span class="footnote">https://people.debian.org/~tomasz/debocker.html</span>がありますが、中身は空っぽです。
debocker(8)を参照するのがよいでしょう。

<section class="warning">
第6章で説明したように、Debian bookwormでunstableのパッケージを開発できるようにしているものとします。ホストがDebian unstableである場合には、バージョンの差異により実行結果が異なることがあります。
</section>

注意としては、名前のよく似た`debdocker`<span class="footnote">https://salsa.debian.org/spog/debdocker</span>というものがありますが別物です。

## debockerを使うメリット

`debocker`を使うメリットは次のことがあげられます。

* bookwormに0.2.5があるのでaptで簡単に導入できる
* `docker`ベースなので、パッケージをビルドするのにホストのroot権限が必要ない
* バンドルと呼ばれるアーカイブを作成できる
* `lintian`を既定で実施してくれる
* 任意のステージからビルドを再開できる

## debockerを使うデメリット

* ビルドした結果を保持しているイメージが区別しづらい

`docker images`でみたときに、`REPOSITORY`や`TAG`で区別がつかない。`<none>`になっているため。

## インストール方法

```cmd
$ sudo apt install -y debocker
```

## 使い方

Debianパッケージのソースディレクトリにて次のコマンドを実行します。

```
~/work $ cd hello-2.10
~/work/hello-2.10 $ debocker build
```

ただし、何もオプションを指定しないと、`docker`コンテナ内にパッケージが保持されます。
ホストに保存するには*--output*オプションを指定します。

```
~/work/hello-2.10 $ debocker build \
  --output ../build-area
```

## git-buildpackageと連携するには

<section class="warning">
第6章のpbuilder+tmpfsな開発環境を整備してある場合に有効です。
</section>

`gbp buildpackage`で`pbuilder`ではなく、`debocker`を使うには、*~/.gbp.conf*の`builder`を次のように変更します。

```
[DEFAULT]
builder = debocker build --output $GBP_BUILD_DIR/..
```

`lintian`はとくに設定しなくても自動的に実行してくれます。
上記のようにすることで、debが*../build-area*配下に生成されます。

設定ファイルのサンプルは本書のリポジトリから入手 <span class="footnote">https://salsa.debian.org/kenhys/debian-new-contributor-guide/-/blob/master/examples/gbp/debocker.conf</span>できます。


## 本章のまとめ

本章では、パッケージのビルドに`debocker`を使う方法を説明しました。

<section class="summary">

* `debocker`を使うと`docker`ベースでパッケージをビルドできる
* `debocker`でビルドしたパッケージをホストに保存したいなら*--output*オプションを忘れずに
* `debocker`を使うと`lintian`も実行してくれる
* `~/.gbp.conf`の設定を切り替えるだけで、`gbp`を使いつつ、パッケージのビルドを`docker`ベースのものに切り替えられる

</summary>

---

<br>
