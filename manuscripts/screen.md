---
class: practice
---

# 付録: LinuxのターミナルをScreenで共有する方法

OSS Gateオンボーディングでは、端末の共有のために`screen`を利用しました。
当初はブラウザから画面共有をしてもらっていたのですが、端末のフォントサイズによってはかなり拡大してもらわないといけない場合があるなど、非常に不便でした。
そのため、端末での操作を共有できると都合が良かったのです。

*owner*ユーザーが*guest*ユーザーとターミナルを共有するセットアップ方法を説明します。

## screenのセットアップをする(初回のみ)

*owner*が*guest*アカウントを作成し、*guest*ユーザーと共有できるようにします。

```cmd:screenで端末を共有できるようにするためのコマンド
$ sudo apt install screen
$ sudo chmod u+s /usr/bin/screen
$ sudo chmod 755 /var/run/screen
$ cp /etc/screenrc ~/.screenrc
$ cat <<EOF >> ~/.screenrc
escape ^Tt
bind windowlist -b
multiuser on
acladd guest
EOF
$ sed -i -e \
  's/^#hardstatus lastline/hardstatus alwayslastline/' \
  ~/.screenrc
```

*~/.screenrc*に記述する`acladd guest`の部分は実際のゲストアカウント名に置き換えてください。

*~/.screenrc*の設定項目の意味は次のとおりです。

|項目|説明|
|---|---|
|`escape ^Tt`|screenのホットキーを`Ctrl+t`にする|
|`bind windowlist -b`|ウィンドウリストを`Ctrl+w`で表示できるようにする|
|`multiuser on`|マルチユーザーを有効にする|
|`acladd guest`|*guest*アカウントに共有を許可する。`acladd debian`なら*debian*ユーザーに共有を許可できます。|


## screenで端末を共有する(*owner*ユーザー側の操作)

設定ができたら、 *owner*ユーザーは次のコマンドを実行して端末を共有できるようにします。

```
owner$ screen -S ossgate
```

これにより、*owner*は*ossgate*という名前のついたセッションを共有します。
*guest*ユーザーには、共有するセッションを教えてあげてください。

## screenで端末を共有してもらう(*guest*ユーザー側の操作)

`screen`で端末を共有してもらう*guest*ユーザー側の操作は次のとおりです。

```
guest$ screen -x owner/ossgate
```

*owner*については実際のユーザー名に置き換えてください。
これで、*guest*ユーザーは*ossgate*セッションに接続できます。

このようにすると、ターミナルを*owner*と*guest*ユーザーが共有して作業できます。

<section class="column">
<h4>screenで端末をうまく共有できないときは</h4>

もし、共有がうまくできないときは、次の項目を確認してください。

■ 設定ファイルの`acladd`の対象ユーザー

*~/.screenrc*の`acladd`の対象ユーザー名が共有対象のユーザー名になっているかを確認してください。

■ パーミッションに関するエラーが発生する

パーミッションに関するエラーとなる場合は、`screen`のバイナリに設定したスティッキービットが解除されていないか確認してください。
スティッキービットを設定しなおすには、次のコマンドを実行します。

```
$ sudo chmod u+s /usr/bin/screen
```

`screen`のパッケージの更新により、スティッキービットがはずれる場合もあるようです。
</section>

