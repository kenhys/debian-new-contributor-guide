# パッケージをアップロードしてみよう

![https://mentors.debian.net/のトップページ](../images/mentors.d.n.png){width=80%}

前章までで、パッケージの体裁を整えるところまでできました。

ここまできたら、次はパッケージをレビューしてもらいましょう。
パッケージをDebian公式のアーカイブに入れてもらうには、スポンサーに所定の場所<span class="footnote">New Queueと呼ばれています。https://ftp-master.debian.org/new.html</span>にアップロードしてもらう必要があります。
アップロードしてもらえたら、あとは審査待ちの状態になります。

パッケージをレビューしてもらうには、そのパッケージをどこか参照できるところで公開しておかないといけません。
そこで、パッケージの一時アップロード先として広く使われているのが https://mentors.debian.net です。

パッケージをアップロードするには次のような手順を踏みます。

* *mentors.debian.net*にアカウントを作成する(初回のみ)
* アップロードに必要な設定をする(初回のみ)
* パッケージに署名する
* パッケージを*mentors.debian.net*にアップロードする

## mentors.debian.netにアカウントを作成する(初回のみ)

まずは*mentors.debian.net*にアカウントを作成します。

![https://mentors.debian.net/の新規登録画面](../images/mentors.d.n.signmeup.png){width=80%}

アカウントの種別は、Maintainerにします。
通知のメールが届くので、メールに記載されているURLにアクセスしてパスワードをリセットします。

次に、パッケージの署名に使うGPGの鍵を登録します。
登録する鍵は次のコマンドを実行して用意します。

```sh:登録に必要な鍵のエクスポートの仕方
$ gpg --export --export-options export-minimal --armor （あなたの鍵のID）
```

なぜ鍵を登録するかというと、誰がパッケージを作成したのか明確にするためです。
署名されていないパッケージは、誰が作ったのかわからないので、*mentors.debian.net*にアップロードしても受理されません。

公開鍵のアップロードはプロフィールのページ<span class="footnote">https://mentors.debian.net/accounts/profile/</span>
から行えます。Change GPG Keyのフォームに貼り付けて、Submitボタンをクリックします。

![https://mentors.debian.net/の鍵登録フォーム](../images/mentors.d.n.publickey.png){width=80%}

アカウントの登録が済んだら、次はアップロードに必要な初期設定をします。

## アップロードに必要な設定をする(初回のみ)

パッケージを*mentors.debian.net*へとアップロードするには`dput`というコマンドを使います。
アップロード先の設定は、`~/.dput.cf`に行います。<span class="footnote">既定では、mentors.debian.netへFTPでアップロードしようとします。FTP使えない環境のことも考慮してHTTPSをおすすめしています。</span>

```conf:~/.dput.cfの設定内容
[mentors]
fqdn = mentors.debian.net
incoming = /upload
method = https
allow_unsigned_uploads = 0
progress_indicator = 2
# Allow uploads for UNRELEASED packages
allowed_distributions = .*
```

これで、パッケージをアップロードするための準備が整いました。
`[mentors]`は単にセクション名なので、好きなものに変更できます。
特に必要なければこのままにしておいてください。以降の説明ではセクション名が`[mentors]`であるとして説明します。

## パッケージに署名する

*mentors.debian.net*にはソースパッケージをアップロードします。
ソースパッケージは署名が必須です。
ソースパッケージに署名するには、次のコマンドを実行します。

```
~/work/build-area $ debsign libfreeaptx_0.1.1-1_source.changes
```

実行中にGPGのパスフレーズを対話的に入力すると、パッケージへの署名が完了します。

```cmd:debsignの実行結果
 unsignfile libfreeaptx_0.1.1-1_source.changes
 unsignfile libfreeaptx_0.1.1-1_source.buildinfo
 unsignfile libfreeaptx_0.1.1-1.dsc
 signfile dsc libfreeaptx_0.1.1-1.dsc

 fixup_buildinfo libfreeaptx_0.1.1-1.dsc libfreeaptx_0.1.1-1_source.buildinfo
 signfile buildinfo libfreeaptx_0.1.1-1_source.buildinfo 

 fixup_changes dsc libfreeaptx_0.1.1-1.dsc libfreeaptx_0.1.1-1_source.changes
 fixup_changes buildinfo libfreeaptx_0.1.1-1_source.buildinfo \
 libfreeaptx_0.1.1-1_source.changes
 signfile changes libfreeaptx_0.1.1-1_source.changes

Successfully signed dsc, buildinfo, changes files
```

次はパッケージをアップロードしましょう。

## パッケージをmentors.debian.netにアップロードする

ソースパッケージのアップロードの際には、引数としてアップロード先であるmentorsと.changesファイルを指定します。

```cmd
~/work/build-area $ dput mentors libfreeaptx_0.1.1-1_source.changes
```

```cmd:dputの実行結果
Checking signature on .changes
gpg: /home/debian/work/libfreeaptx_0.1.1-1_source.changes: 
Valid signature from 167F087D1684187C
Checking signature on .dsc
gpg: /home/debian/work/libfreeaptx_0.1.1-1.dsc: Valid signature from 167F087D1684187C
Uploading to mentors (via https to mentors.debian.net):
  Uploading libfreeaptx_0.1.1-1.dsc: done.
  Uploading libfreeaptx_0.1.1.orig.tar.gz: done.  
  Uploading libfreeaptx_0.1.1-1.debian.tar.xz: done.
  Uploading libfreeaptx_0.1.1-1_source.buildinfo: done.
  Uploading libfreeaptx_0.1.1-1_source.changes: done.
Successfully uploaded packages.
```

アップロードに成功してしばらくすると、パッケージごとの個別ページ<span class="footnote">https://mentors.debian.net/package/(パッケージ名)/</span>が参照できます。
パッケージのページでは`lintian`によるチェック結果を参照できます。 

たとえば、個別ページのQA informationの欄に次のように表示されます。これがグリーンな状態を目指しましょう。

![QA information](../images/mentors.d.n.qa.png)

他にもログインしていれば、パッケージに関してレビューコメントをしたりできます。スポンサーとのやりとりに使うとよいでしょう。

## 本章のまとめ

本章では、*mentors.debian.net*の使い方を説明しました。

<section class="summary">

* mentors.debian.netにアカウントが必要
* mentors.debian.netにGPG鍵を登録する必要がある
* mentors.debian.netにはソースパッケージをアップロードする

</section>

次章では、スポンサーを探す方法を説明します。

<section class="column">

<h4>もしアップロード先を間違えたらどうすればいいのか?</h4>

もし`dput`したときに、アップロード先としてmentorsの指定を忘れるとどうなるでしょうか。
つまり、セクション名の指定を忘れて`dput *.changes`を実行してしまった場合です。
そのときは`dput`のデフォルトのアップロード先にアップロードされてしまいます。
`dput`のデフォルトのアップロード先は[ftp-master]です。これはDebian開発者やDebianメンテナーが使うアップロード先です。

その場合には、`dput`パッケージに含まれている`dcut`コマンドを使って取り消します。

```
$ dcut -m (GPG公開鍵に紐付いたメールアドレス) \
  -i (アップロードしたときに指定した.changesファイル)
```

`dcut`を使うとファイルの削除を指示するコマンドファイルというものを自動的に作成して、サーバーにアップロードします。サーバー側では、定期的にアップロードされたコマンドファイルを処理しているので、しばらくするとコマンドファイルに従ってファイルを削除してくれます。
48時間を超えて適切に処理がなされていないものは自動的に削除されるようになってはいるので、`dcut`で削除するのがオススメではありますが、それほど神経質にならなくてもよいでしょう。

</section>
