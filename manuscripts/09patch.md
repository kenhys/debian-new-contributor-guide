# パッケージの問題をフィードバックしよう

本章では、パッケージング中にバグを踏んだりした場合にフィードバックする方法を説明します。

## パッケージング中に見つかる問題とは

Debianでは、幅広い環境をサポートしています。
そのため、特定の環境でビルドできないということがあります。

Debianパッケージ側でパッチをあてることもできますが、パッチのメンテナンスをしていくのも大変です。
アップストームにとりこんでもらうほうが、将来的なメンテンスコストを下げられます。

そういう場合はぜひアップストリームにフィードバックしておきましょう。<span class="footnote">ただし、マイナーすぎてアップストリーム側でもメンテナンスできないので、パッチをフィードバックしても受け入れてもらえないということもあります。</span>

## 既存のパッケージのバグを見つけたら

パッケージングをしていて、既存のパッケージのバグを見つける、ということもあります。
その場合は、`reportbug`を使ってバグを報告します。

```
$ reportbug --output=report.txt
```

第5章のITPするときはwnppというオプションをつけていましたが、バグ報告のときは不要です。
MTAの設定がなければ、そのままメール送信できません。
`--output`オプションを指定していったんファイルに保存して、あとでメールクライアントにコピペして送信します。

`reportbug`を使ったバグ報告の流れは次のとおりです。

* パッケージ名を入力する
* 既存のバグリストが表示されるので、該当するものがないか探す
* 報告するバグの内容のサマリーを一行で入力する
* 報告するバグの重要度をimportant/normal/minor/wishlistから選択する
* Subject:と報告内容の雛形を埋める

雛形では、次の内容を最低限記述する必要があります。

```
* What led up to the situation?
* What exactly did you do (or not do) that was effective (or
  ineffective)?
* What was the outcome of this action?
* What outcome did you expect instead?
```

バグの発生した状況、何をしたらそうなったのか、回避策があるのか否か。
実行結果はどうだったのか、期待する結果はどんなものだったのか、などです。

## Debianパッケージでのパッチの扱い方

アップストリームにパッチがとりこまれるまでは、やむなくDebianパッケージ側で対処することになります。

Debianパッケージでパッチをあてる場合、*debian/patches*配下にパッチファイルを配置します。
パッチ関連のファイルは次のようになっています。

* *debian/patches/series* パッチをリストアップしたファイル
* *debian/patches/xxxx.patch* パッチファイル

*debian/patches/series*にはどのパッチファイルをどの順番であてるかを記述します。

パッチにはメタ情報を含めることが推奨されています。
これは、パッチがどんな状態にあるのか、そのままではわからないからです。
したがって、アップストリームにフィードバック済みなら、そのURLを`Forwarded`フィールドの値として指定します。

パッチファイルのルールについては[DEP-3](https://dep-team.pages.debian.net/deps/dep3/)<span class="footnote">https://dep-team.pages.debian.net/deps/dep3/</span>としてまとめられています。

## git-buildpackageと連携するには

`gbp`にはサブコマンドとしてパッチをあつかうための`pq`があります。

すでにリポジトリにパッチを含めているなら、次のコマンドを実行します。

```
$ gbp pq import
```

まだリポジトリにパッチを含めていないなら、次のコマンドを実行します。


```
$ gbp pq rebase
```

すると、パッチをあつかうための専用のブランチに切り替わります。<span class="footnote">`patch-queue/debian/unstable`あるいは`patch-queue/debian/sid`。gbp.confのdebian-branchの設定によっても変わる</span>

この専用のブランチにコミットを積み重ねることでパッチを更新していきます。

パッチをすべて更新し終えたら、エクスポートします。
パッチをエクスポートするには、次のコマンドを実行します。

```
$ gbp pq export
```

すると、既定のブランチ(例えば`debian/unstable`ブランチ)に戻ります。
*debian/patches*配下に`patch-queue/*`ブランチで積み上げたコミットがパッチファイルとして出力されます。

エクスポートされたパッチをコミットするのを忘れないでください。

## パッチに含めるべき情報

DEP-3<span class="footnote">https://dep-team.pages.debian.net/deps/dep3/</span>に従うならば、
次のような情報をコミットに含める必要があります。

|項目|必須/任意|説明|
|---|---|---|
|SubjectもしくはDescription|必須|パッチの内容を説明する|
|Origin| (Authorがない場合必須)|パッチの出自を示すURL|
|Author:もしくはFrom|任意|パッチの作者を指定します|
|BugもしくはBug-<Vendor>|任意|upstreamのバグ報告先のURL。Deianのバグ報告先ならBug-DebianにURLを指定します|
|Forwarded |任意|"no"もしくは"not-needed"を指定する。Debian特有であり、upstreamに報告しても取り込まれる見込みがない場合には"no-needed"を選択します。"no"はまだフィードバックしていないことを意味します|
|Reviewed-byもしくはAcked-by|任意|パッチをレビューしてもらうなり承認してもらった場合に指定します|
|Last-Update |任意|YYYY-MM-DD形式でメタ情報を付与します|
|Applied-Upstream|任意|Upstreamにパッチが適用されたバージョンだったり、URL、コミットIDなんかを指定します|


gbp pqでパッチをエクスポートすると、パッチファイル名は自動生成されます。
自動生成されるパッチファイル名でなく、一意なファイル名を保ちたいなら、
コミットメッセージにひと工夫することで、任意のファイル名にすることができます。

次のようなコミットメッセージが含まれていると指定したファイル名でパッチが作成されます。

```
Gbp-Pq: Name (パッチのファイル名)
```

Gbp-Pq:タグについては[gbpを使ったDebianパッケージでパッチのファイル名を明示的に指定するには](https://www.clear-code.com/blog/2020/7/22.html)
<span class="footnote">https://www.clear-code.com/blog/2020/7/22.html</span>を参考にしてみてください。

## 本章のまとめ

本章では、パッケージの問題をフィードバックする方法について説明しました。

<section class="summary">

* まずはアップストリームにフィードバックすることを考えよう
* 既存のバグの報告は`reportbug`を使おう
* どうしてもパッチを抱え込む必要があるなら`gbp pq`で管理しよう

</section>

次の章では、体裁を整えたパッケージをアップロードする方法を説明します。

---

<br>
