# よりよいDebianパッケージにしてみよう

前章までで、テンプレートをもとにそれらしいパッケージへと修正をすすめてきました。
実際に次のコマンドでパッケージをビルドできます。

```
~/work/libfreeaptx $ gbp buildpackage --git-ignore-new
```

*../build-area*ディレクトリを確認するとパッケージがビルドできていることがわかります。

```
~/work/libfreeaptx $ tree ../build-area
../build-area
├── freeaptx-utils_0.1.1-1_amd64.deb
├── libfreeaptx-dev_0.1.1-1_amd64.deb
├── libfreeaptx0_0.1.1-1_amd64.deb
├── libfreeaptx_0.1.1-1.debian.tar.xz
├── libfreeaptx_0.1.1-1.dsc
├── libfreeaptx_0.1.1-1_amd64.build
├── libfreeaptx_0.1.1-1_amd64.buildinfo
├── libfreeaptx_0.1.1-1_amd64.changes
├── libfreeaptx_0.1.1-1_source.changes
└── libfreeaptx_0.1.1.orig.tar.gz
```

パッケージができただけでは、よいDebianパッケージであるとはいえません。
いくつかやっておくべきことがあるのでそれを説明します。

* `lintian`の指摘する問題を修正しよう
* パッケージのインストールテストをしよう
* パッケージを自動的にテストをしよう(`autopkgtest`)

## lintianの指摘する問題を修正しよう

Debianにはパッケージングに関する指針が定められており、これはDebian Policy Manual<span class="footnote">https://www.debian.org/doc/debian-policy/</span>としてまとめられています。

しかし、定められているポリシーは多岐にわたり、すべてを把握して適切にパッケージングするのはなかなか大変です。
そこで、チェックを支援するツールが提供されています。
これが`lintian`というツールです。

第6章では*pbuilder*の設定をしたときに、いっしょに`lintian`の設定を追加しました。
そのため、すでにパッケージをビルドしたときに、`lintian`も実行するようになっています。
ログは多岐に渡るため、ビルド時に`2>&1 | tee ../build.log`などとしてリダイレクトして残しておくとよいでしょう。

まずはサマリー(抜粋)を確認してみましょう。

ログの`+++ lintian output +++`から`+++ end of lintian output +++`までを参照してください。


```text:~/work/build.log.log
E: libfreeaptx source: debian-rules-is-dh_make-template
W: freeaptx-utils: no-manual-page usr/bin/freeaptxdec
W: freeaptx-utils: no-manual-page usr/bin/freeaptxenc
I: libfreeaptx source: debian-watch-contains-dh_make-template Example watch control \
  file for uscan
I: freeaptx-utils: hardening-no-bindnow usr/bin/freeaptxdec
I: freeaptx-utils: hardening-no-bindnow usr/bin/freeaptxenc
I: libfreeaptx0: hardening-no-bindnow usr/lib/x86_64-linux-gnu/libfreeaptx.so.0.1.1
I: libfreeaptx0: no-symbols-control-file usr/lib/x86_64-linux-gnu/libfreeaptx.so.0.1.1
```

`lintian`の結果のフォーマットは次のようになっています。

```
種別: パッケージ: タグ 説明
```

種別には次のようなものがあります。

| 種別 | 説明                                                                                                                                                                         |
|------|----------------------------------------------------------------------------------------------------------------------------------------|
| E:   | エラーあり。ポリシー違反あるいはパッケージングに問題があることを示す。パッケージを修正すべきです。|
| W:   | 警告あり。ポリシー違反あるいはパッケージングに問題がある可能性を示す。修正が望ましいですが、誤検出ということもありえます。誤検出が明らかな場合には、抑制することもできます。|
| I:   | 情報あり。よりよいパッケージにするヒントであることもあるので、修正するのがオススメ。|
| N:   | デバッグメッセージです。|
| X:   | 十分にテストされていない実験的なヒントを示す。|
| O:   | 明示的にルールが抑制されたことを示す。|
| P:   | 潜在的な問題を指摘していることがあるが、必ずしもそのとおりに修正するのがよいとは限らない。|

W:以上は修正したほうが望ましいでしょう。可能なら、I:も修正するのをおすすめします。

それでは、`libfreeaptx`の場合を例に、それぞれの`lintian`のメッセージにどのように対処するのがよいのか説明します。

### E: libfreeaptx source: debian-rules-is-dh_make-template

`debian-rules-is-dh_make-template` は`dh_make`で生成したテンプレートがそのまま残っていると発生します。

*debian/rules*から不要な部分を削除しましょう。
最低限必要なのは次の部分です。

```text:~/work/libfreeaptx/debian/rules
#!/usr/bin/make -f

%:
        dh $@

override_dh_auto_install:
        dh_auto_install -- PREFIX=/usr LIBDIR=lib/$(DEB_HOST_MULTIARCH)
```

### W: freeaptx-utils: no-manual-page usr/bin/freeaptxdec

`no-manual-page`とあるとおり、*/usr/bin/freeaptxdec*に対するmanページがありません。
`pandoc`を使うとMarkdownからmanページを生成できます。
*/usr/bin/freeaptxenc*のときと同じように、`freeaptxdec --help`の結果を参考に次のような*freeaptxdec.md*を用意します。

```text:~/work/libfreeaptx/debian/freeaptxdec.md
% freeaptxdec(1) | User Commands
%
% "October 15 2021"

# NAME

freeaptxdec - aptX decoder utility

# SYNOPSIS

**freeaptxdec** [**options**]

# DESCRIPTION

This utility decodes aptX or aptX HD audio stream from stdin to a raw 24 bit signed
stereo on stdout

When input is damaged it tries to synchronize and recover

Non-zero return value indicates that input was damaged and some bytes from input
aptX audio stream were dropped

# OPTIONS

The program follows the usual GNU command line syntax, with long options starting
 with two dashes (`-'). A summary of options is included below.

**-h**, **\-\-help**
:   Display this help

**\-\-hd**
:   Decode from aptX HD

# BUGS

The upstream BTS can be found at https://github.com/iamthehorker/libfreeaptx/issues/new.

# SEE ALSO

**freeaptxenc**(1)

# AUTHOR

Kentaro Hayashi <kenhys@xdump.org>:   Wrote this manpage for the Debian system.

# COPYRIGHT

Copyright © 2021 Kentaro Hayashi

This manual page was written for the Debian system (and may be used by others).

Permission is granted to copy, distribute and/or modify this document under the
 terms of the GNU General Public License, Version 2 or (at your option)
any later version published by the Free Software Foundation.

On Debian systems, the complete text of the GNU General Public License can be found in
/usr/share/common-licenses/GPL.
```

次のコマンドを実行すると、manページに変換できます。

```
~/work/libfreeaptx/debian $ pandoc --standalone --from=markdown --to=man \
  freeaptxdec.md --output=freeaptxdec.1
```

manページを`freeaptx-utils`パッケージと一緒にインストールするには、次のような`freeaptx-utils.manpages`を追加します。

```text:~/work/libfreeaptx/debian/freeaptx-utils.manpages
debian/freeaptxdec.1
```

### W: freeaptx-utils: no-manual-page usr/bin/freeaptxenc

これも`no-manual-page`とあるとおり、*/usr/bin/freeaptxenc*に対するmanページがありません。
manページを追加することで解決します。

同様にして、`freeaptxdec`もインストールするように*freeaptx-utils.manpages*に追加します。

```text:~/work/libfreeaptx/debian/freeaptx-utils.manpages
debian/freeaptxdec.1
debian/freeaptxenc.1
```

### I: libfreeaptx source: debian-watch-contains-dh_make-template Example watch control file for uscan

`debian-watch-contains-dh_make-template`は*debian/watch*にテンプレートがそのまま残っていると発生します。

version=の行とopts=の行を残してあとはすべて削除します。

```
version=4
opts=filenamemangle=s/.+\/v?(\d\S+)@ARCHIVE_EXT@/@PACKAGE@-$1@ARCHIVE_EXT@/ \
  https://github.com/iamthehorker/libfreeaptx/tags .*/v?(\d\S+)@ARCHIVE_EXT@
```

### I: freeaptx-utils: hardening-no-bindnow usr/bin/freeaptxdec

`hardening-no-bindnow`は、Hardening<span class="footnote">https://wiki.debian.org/Hardening</span>と呼ばれる実行するバイナリーをよりセキュアにする仕組みが適用されていないことを示します。

有効にするには、まず`DEB_BUILD_MAINT_OPTIONS=hardening=+all`を*debian/rules*に設定します。
hardeningにはいくつか種類があるのですが、`hardening=+all`はすべて有効にすることを意味します。
問題があれば、適用するルールを調整するとよいでしょう。<span class="footnote">プラグインに対応しているソフトウェアなどでは、`hardening=+all`を有効にしているとプラグインの.soがロードできなくて問題になるということがあったりします。</span>

だいたい上記を定義するだけでよいのですが、`libfreeaptx`の場合は、*Makefile*を環境にあわせて生成する仕組みになっていません。
そのままだとビルドフラグに適切なものが使われません。
そこで、ビルドのときのルールを上書きして、適切なビルドフラグ等を設定します。

<!--
<section class="column">
<h4>dhとはなんなのか</h4>

`debhelper`というツール含まれるコマンドです。
ビルドをいい感じにやってくれるという便利ツールです。`dh_xxx`というのをたくさん裏で呼び出します。
が
</section>
//-->

ビルドのときのルールは`dh_auto_build:`が適用されるので、`override_dh_auto_build`を定義することで上書きします。
`$(shell dpkg-buildflags --get CFLAGS)`は実行環境に適したビルドフラグを取得するためのものです。

```text:~/work/libfreeaptx/debian/rules
#!/usr/bin/make -f
#export DH_VERBOSE = 1
export DEB_BUILD_MAINT_OPTIONS = hardening=+all

%:
        dh $@

override_dh_auto_build:
    dh_auto_build -- CFLAGS="$(shell dpkg-buildflags --get CFLAGS)" \
      LDFLAGS="$(shell dpkg-buildflags --get LDFLAGS)"

override_dh_auto_install:
    dh_auto_install -- PREFIX=/usr LIBDIR=lib/$(DEB_HOST_MULTIARCH)
```


### I: freeaptx-utils: hardening-no-bindnow usr/bin/freeaptxenc

これもHardening関連です。
`freeaptxdec`で実施した修正により問題は解消します。

### I: libfreeaptx0: hardening-no-bindnow usr/lib/x86_64-linux-gnu/libfreeaptx.so.0.1.1

これもHardening関連です。
`freeaptxdec`で実施した修正により問題は解消します。

### I: libfreeaptx0: no-symbols-control-file usr/lib/x86_64-linux-gnu/libfreeaptx.so.0.1.1

`no-symbols-control-file`はシンボルファイルがないことを示します。

シンボルファイルは、ABIの変更をトラッキングするためのファイルです。
シンボルの増減で互換性がなくなった場合には、`libfreeaptx0`を`libfreeaptx1`などと変更したりします。

シンボルファイルを生成するには、`dpkg-deb`を使ってsymbolファイルを抽出するのが簡単です。

```
~/work/libfreeaptx $ dpkg-deb -R ../build-area/libfreeaptx0_0.1.1-1_amd64.deb \
  /tmp/libfreeaptx0
~/work/libfreeaptx $ sed -e 's/0\.1\.1-1/0\.1\.1/' /tmp/libfreeaptx0/DEBIAN/symbols \
  | tee debian/libfreeaptx0.symbols
```

<section class="column">
<h4>lintianのメッセージがよくわかりません</h4>

`lintian`が検出してくれるタグ一覧はウェブから参照できる <span class="footnote">https://udd.debian.org/lintian-tags/ でタグを参照できる。以前は https://lintian.debian.org/tags というのがありましたが廃止されました。</span>ようになっています。

`lintian`はポリシーに準拠しているかを教えてくれますが、必ずしもどう修正すればいいのかを丁寧に教えてくれるわけではありません。

`lintian`の`--info`オプションを指定すれば、ある程度詳細が出力されます。ポリシーのどの部分に準拠していないかを知ることもできます。

そんなときは、次のように調べていくとよいでしょう。

* [Debian Policy](https://www.debian.org/doc/debian-policy)<span class="footnote">https://www.debian.org/doc/debian-policy</span>の該当箇所を熟読する
* [Debian デベロッパーレファレンス](https://www.debian.org/doc/manuals/developers-reference/)<span class="footnote">https://www.debian.org/doc/manuals/developers-reference/</span>を検索する
* `lintian`の表示してくれたキーワードをヒントに https://wiki.debian.org を検索する
* Debianのソースコードを[Debian Code Search](https://codesearch.debian.net/) <span class="footnote">https://codesearch.debian.net/</span>で検索する。他のパッケージがどうやって問題を修正したのかがわかったりする
* mentorsのメーリングリストを検索する
</section>

<!-- TODO: B90lintianのオプションを -EvIL +pedanticに変更するべき -->

## パッケージのインストールテストをしよう

パッケージを作れるようになりましたが、そのパッケージがきちんとインストール・アンインストールできるかはまだ確認していません。

そのような目的で使うのが`piuparts`です。パッケージのインストール・アンインストールを行い、問題がないかを確認できます。

`piuparts`でパッケージをテストするには次のコマンドを実行します。

```
~/work $ sudo piuparts  --basetgz=/var/cache/pbuilder/base.tgz build-area/*.deb
```

`piuparts`による、インストール・アンインストール・アップグレードのテストに成功すると、最後に次のような結果が表示されます。

```
1m12.8s INFO: PASS: All tests.
1m12.8s INFO: piuparts run ends.
```

上記のように`PASS: All tests`という結果であれば、ひとまずインストールに関連する地雷はないはずです。

もし、パッケージのビルドで第6章で説明した`tmpfs`を使う<span class="footnote">*/var/cache/pbuilder/build*を`tmpfs`としてマウント</span>ようにしているなら、`piuparts`でもそれを指定すると速く終わります。

```
~/work $ sudo piuparts -t /var/cache/pbuilder/build \
  --basetgz=/var/cache/pbuilder/base.tgz build-area/*.deb
```

なお、まれではありますが、`piuparts`実行中に依存先のパッケージでエラーが発生することもあります。
`pbuilder`のイメージを更新しても再現するようなら、該当パッケージにバグ報告しましょう。
報告先は`submit@bugs.debian.org`です。
バグ報告の仕方については、第9章にて説明しています。

## パッケージを自動的にテストをしよう(autopkgtest)

`piuparts`はインストール関連のテストに限られます。そのパッケージが期待通りに動作するかどうかは関知しません。

パッケージの動作確認のために、自動テストを実行するには、`autopkgtest`という仕組みがあります。

Ubuntu Weekly Recipeの第311回で詳しく説明されているので、参考にしてみてください。

[第311回　autopkgtestでパッケージのテストを自動化する](https://gihyo.jp/admin/serial/01/ubuntu-recipe/0311)<span class="footnote">https://gihyo.jp/admin/serial/01/ubuntu-recipe/0311</span>

第6章の`pbuilder`の設定で、`autopkgtest`を既定で有効にしてあります。

そのため`gbp buildpackage`を実行したときに(テストファイルが用意してあれば)テストを実行できるようになっています。

### テストファイルを追加する

テストを追加するには、次のファイルを用意します。

* *debian/tests/control* テストのレシピを記述したファイル
* *debian/tests/xxxx* 任意のテスト手順を記述したファイル

```text:~/work/libfreeaptx/debian/control
Tests: encode decode
Depends: sox festival freeaptx-utils libfreeaptx0
```

`Tests:`には任意のテスト手順を記述したファイルの名前を指定します。
`Depends:`にはテストを実行するときに必要なパッケージを記述します。


例えば、*libfreeaptx/debian/tests/encode*を用意して、freeaptxencの実行結果をテストしたり、*libfreeaptx/debian/tests/decode*を用意して、freeaptxdecの実行結果をテストしたりします。

### テストを実行する

テストファイルを追加した状態で`gbp buildpackage`で再度パッケージをビルドすると、次のようにテストが実行されます。

```
Removing autopkgtest-satdep (0) ...
...
encode               PASS
decode               PASS
autopkgtest [13:10:14]: Binaries: resetting testbed apt configuration
```

テスト実行時に終了コードとして0でないものが返ってくると、テストに失敗したとみなします。

```
Removing autopkgtest-satdep (0) ...
...
encode               FAIL non-zero exit status 1
decode               PASS
autopkgtest [13:11:52]: Binaries: resetting testbed apt configuration
```

---

## 本章のまとめ

本章では、よりよいDebianパッケージにするため、`lintian`を活用して体裁を整える方法や、インストール関連、自動テストについて説明しました。

<section class="summary">

* `lintian`を活用するとパッケージをいい感じにできる
* `piuparts`を利用することでインストール関連の不具合を潰せる
* `autopkgtest`を整備すると動作確認を自動化できる

</section>

次章では、パッケージの問題のフィードバックやパッチについて説明します。

