
<!-- force page break after toc -->

<br>

---


# はじめに

2021年8月から10月にかけて、OSS Gateオンボーディング <span class="footnote">https://oss-gate.github.io/on-boarding</span> を実施しました。
OSS Gateオンボーディングというのは、「オンボーディング」とあることからもわかるように、新しく入ってきた人が組織になじんで活躍してもらえるようにもろもろ支援する取り組みです。
対象が特定のOSSであるところが、一般的なオンボーディングとは異なります。

筆者はDebianコミュニティーに新しく継続的に参加してくれる人が増えるとよいなということで、第1回のOSS Gateオンボーディングにて先輩役を務めました。
そのときの内容については最終レポート「[OSS Gateオンボーディング第一回を実施 - Debianコントリビューターを1人増やせた！](https://oss-gate.github.io/report/on-boarding/2021/10/08/on-boarding-2021-08-kenhys.html)」<span class="footnote">https://oss-gate.github.io/report/on-boarding/2021/10/08/on-boarding-2021-08-kenhys.html</span>
として公開しています。
新人さん視点の最終レポートも「[debianのパッケージングができるようになった夏](https://oss-gate.github.io/report/on-boarding/2021/11/20/on-boarding-2021-08-sivchari.html)」<span class="footnote">https://oss-gate.github.io/report/on-boarding/2021/11/20/on-boarding-2021-08-sivchari.html</span>
として公開されています。

OSS Gateオンボーディングの支援期間中には、週1回新人さんと一緒に作業する時間をとって新規Debianパッケージ化の作業をしました。
新人さんと話していると、こんなときどうするといいのか？といった質問や、どうしてそうなっているのか？という疑問が寄せられました。

Debianプロジェクトの活動は基本的にオープンなので、質問や疑問については、じっくり調べてみることで(おそらく)解決できるでしょう。
しかし、そうはいってもDebian特有の事情だったり背景については、誰かまわりに知っている人がいないとつらい場合もありそうです。

OSS Gateオンボーディングという取り組みは、先輩役がひとりの新人さんのメンターとなるしくみです。
OSS Gateワークショップ<span class="footnote">OSS Gateというプロジェクトが実施しているワークショップのこと。OSSの開発に参加するために最初の敷居を下げるために、ワークショップで実際に手を動かしてもらう体験をしてもらっている。</span>ほど多くの人をまとめて面倒みることはできません。
そのため、先輩役を務められる人がたくさんいたり、スポンサーがいないと、なかなかうまくまわっていきません。
OSS Gateオンボーディングの理念に共感し、支援してくれるスポンサーを探しながらとなると、すぐには規模を拡大していくのは難しいでしょう。
<span class="footnote">実際、OSS Gateオンボーディングは第2回の「MariaDB Server のバグ修正および機能開発」以降開催されていません。</span>

そこで、第1回のOSS Gateオンボーディングで得られた知見や普段のDebian関連の作業を通じて得た知識をまとめて、薄い本として配布してみることにしました。
このやりかたならば、Debianのローカルコミュニティーの1つであるDebian勉強会や、OSS Gateオンボーディングに参加できない人にも知見を伝えることができます。

この薄い本が将来のDebianコミュニティーに関わりたい新人さんの助けになることを願っています。
