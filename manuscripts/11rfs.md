# パッケージのスポンサーを探し(RFS)てみよう

パッケージの体裁が整ったところで、mentors.debian.netにパッケージをアップロードするところまで進みました。

以前説明したように、Debian Contributorはパッケージを直接Debian公式のアーカイブにアップロードできません。
スポンサーしてもらう必要があります。RFSとはRequest For Sponsorの略です。
本章ではスポンサーの見つけ方を説明します。

## スポンサーの見つけ方

スポンサーを見つけるための方法はいくつかあります。

* mentors.debian.netにアップロードしたパッケージの`Need a sponsor`フラグをたてて、誰かがスポンサーしてくれるのを待つ
* RFSをバグとして登録する(debian-develメーリングリストにメールが流れる。Debianの標準的な方法。)
* RFSしても反応がないなら、debian-mentors@lists.debian.orgにスポンサー依頼のメールを投げる(RFSしたときにもdebian-mentors@lists.debian.orgにメールが流れているはずなので、念押しする感じ。)
* debian-devel@debian.or.jpにスポンサーを依頼するメールを投げる
* Debian勉強会でスポンサーをお願いする(依頼先のDebian開発者が忙しくなければ受けてもらいやすいかも)

最後のが確実ではありますが、いずれにせよコミュニケーションを必要とします。

## mentors.debian.netでNeed a sponsorするには

mentors.debian.netのパッケージリストのページをみていると、Needs a sponsorという欄があります。
スポンサーを必要としているパッケージであることを示します。

![Need a sponsorの有無を示す画面](../images/mentors.d.n.pkglist.png){witdh=80%}

これを設定するには、第10章で説明した、パッケージをアップロードして受理されたあとに生成されるパッケージ個別のページで設定をします。

メリットとしては、設定が簡単であるということです。
デメリットとしては、mentors.debian.netをこまめに見にくるような人しか気づかないということです。

## mentors.debian.netを利用してRFSメールを投げるには

より気づいてもらいやすくするために、RFSをバグとして登録します。
RFSのメールにもお作法があります。

mentors.debian.netにはRFSのテンプレートを生成してくれるというすてきな機能があります。
それを活用しましょう。
アップロードしたパッケージから情報をかき集めてテンプレートを生成してくれます。

![View RFS templateのリンク画面](../images/mentors.d.n.rfstemplate.png){width=80%}

RFSのテンプレートは、パッケージが受理されたあとのパッケージ個別のページ(mentors.debian.netへログインが必要)にあります。
バージョンの脇のあたりにView RFS templateというリンクが生成されるので、そのリンクを踏むとテンプレートが表示されます。

あとは、もしまだ[fill in]となっているところがあれば埋めて、テキストをコピペして`submit@bugs.debian.org`にメールするだけでRFSは完了です。

## Debian勉強会でスポンサーを探す

Debianに関するローカルコミュニティーとして、[東京エリアDebian勉強会](https://tokyodebian-team.pages.debian.net/)<span class="footnote">https://tokyodebian-team.pages.debian.net/</span>が毎月1回のペースで開催されています。

昨今の状況もあり、最近はもっぱらオンラインで開催されています。
Debian開発者が参加していることも多いので、そこでスポンサーをお願いするのも有効です。

---

<section class="column">
<h4>パッケージングの肝はスポンサー探しにあり</h4>

Debian開発者があるパッケージをスポンサーするかしないかは任意です。
したがって、パッケージをDebian公式アーカイブにすんなり入れられるかどうかの最初の関門は、いかにスポンサーを見つけられるかにかかっています。

パッケージング作業に関しては、ひとりでもくもくと進められますが、スポンサー探しはソーシャルスキルが必要とされます。

</section>

## 本章のまとめ

本章では、RFSをだしてパッケージのスポンサーをつのるところを説明しました。

<section class="summary">

* パッケージをmentors.debian.netにアップロードしたあとはスポンサーを確保する必要がある
* RFSのメールをだすときは、mentors.debian.netのView RFS templateを活用するとよい
* RFSをだしてもすぐにスポンサーがつくとは限らない、急ぐならDebian勉強会でDebian開発者をつかまえるのがおすすめ

</section>

次章では、残念ながらパッケージがrejectされてしまった場合にどうするとよいかを説明します。

