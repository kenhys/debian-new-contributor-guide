---
class: practice
---

# 実践: ConoHaで開発環境を構築する

国内にて時間単位で利用できるVPSを提供しているサービス提供事業者の1つが[ConoHa](https://www.conoha.jp/vps/)<span class="footnote">https://www.conoha.jp/vps/</span>です。
メモリ2GBのプランなら、3.7円/時で利用できます。<span class="footnote">V3のインスタンスの2024年11月時点での情報。</span>

ConoHaを使う場合、標準でイメージを50GBまで追加費用なく保存できます。
VPSインスタンスをイメージとして保存できるので、作業環境を常に起動しておく必要はありません。
必要なときに、保存したイメージからVPSインスタンスを再構築<span class="footnote">再構築なので当然のことながらIPアドレスは割り当て直されます。</span>できます。

VPSを起動しっぱなしだとコストがかかりますが、イメージの保存では追加費用がかからないので、OSS Gateオンボーディングのように週1回一緒に作業するような場合にはおすすめです。

## 前提条件

VPSが使えるのであれば、最初からunstableの環境を構築するのが手っ取り早いでしょう。
本章では、ConoHaにunstableの環境を構築する手順を説明します。

* インスタンスの選択
* Debian 12.7への更新

なお、第6章と同様にホストに開発関連のパッケージをインストールしてビルドする手順を説明します。

## インスタンスの選択

作業環境のスペックとしてはたいていはメモリ2GB CPU 3コア,SSD 100GBのプランであれば事足ります。
よりディスクやメモリがビルドするときに必要なソフトウェアの作業をする場合には、スペックをあげることを検討してください。
初期選択可能なのがDebian 12.5 <span class="footnote">2024年11月時点での情報。12.7までのポイントリリースは適用されていない。</span> なのでいったん最新にしてからunstableに更新する手順です。

なお、ConoHaはVer 2.0とVer 3.0の2つのインフラ基盤を選択することができますが、通常はVer 3.0のインフラ基盤でサーバーの追加を推奨します。
<span class="footnote">2024年11月時点でVer 2.0のインフラ基盤だとポイントリリースの適用されていない、Debian 12.0やDebian 11.0しか選択できないため。</span>

Ver 3.0のインフラ基盤でサーバーを追加するときには、オプションのセキュリティグループの設定で、IPv4v6-SSHをあらかじめ適用しておかないと、SSHでサーバーにログインすることができません。
また、オプションでSSH Keyの設定を忘れずにしておきましょう。

## サーバーへのログイン

追加したサーバーのIPを確認し、次のようにコマンドを実行してログインします。

```
ssh -i サーバー構築時に指定した公開鍵 root@(サーバーのIPアドレス)
```

初期ユーザーは作成されていないので、適当な作業用ユーザーを作成しておきましょう。
例えば、`debian`ユーザーを追加するには次のコマンドを実行します。

```
useradd -m -s /bin/bash debian
gpasswd -a debian sudo
mkdir -p /home/debian/.ssh
cp /root/.ssh/authorized_keys /home/debian/.ssh/
chown debian:debian /home/debian/.ssh -R
passwd debian
```
最後に`passwd`コマンドで`debian`ユーザーのパスワードを設定しておきます。

これ以降は追加した作業用のユーザー`debian`で操作するものとします。

## Debian 12.7への更新

Debian 12.7に更新するには、次のコマンドを実行します。

```
$ sudo apt update
$ sudo apt upgrade -y
```

これでポイントリリースが適用され、最新のDebian 12.7になっているはずです。

## /etc/apt/sources.listの更新

次に、`/etc/apt/sources.list`の内容をunstableに変更します。

```変更前
cat sources.list
#deb cdrom:[Debian GNU/Linux 12.0.0 _Bookworm_ - Official amd64 DVD Binary-1 with firmware 20230610-10:23]/ bookworm main non-free-firmware
#
deb http://deb.debian.org/debian/ bookworm main
deb-src http://deb.debian.org/debian/ bookworm main non-free-firmware
#
deb http://security.debian.org/debian-security bookworm-security main
deb-src http://security.debian.org/debian-security bookworm-security main
#
## bookworm-updates, to get updates before a point release is made;
## see https://www.debian.org/doc/manuals/debian-reference/ch02.en.html#_updates_and_backports
deb http://deb.debian.org/debian/ bookworm-updates main
deb-src http://deb.debian.org/debian/ bookworm-updates main
```

変更後の`/etc/apt/sources.list`は次のとおりです。


```変更後
deb http://deb.debian.org/debian/ unstable main
deb-src http://deb.debian.org/debian/ unstable main
```

## Debian unstableへの更新

次のコマンドを実行して、unstableのパッケージへと更新します。

```
$ sudo apt update
$ sudo apt upgrade -y
$ sudo apt dist-upgrade -y
$ sudo apt autoremove -y
```

なお、Debian bookwormからunstableにパッケージを更新する際、パッケージ名にt64というサフィックスのついたパッケージに
置き換えられます。これは2038年問題に対処するために行われた大規模な修正が適用されたためです。

途中でgrubやntpについて設定を維持するか確認されますが、メンテナーによる新しい設定ファイルに置き換えてしまって
問題ありません。


## 開発に必要なパッケージのインストール

パッケージング関連で必要なものを、次のコマンドを実行してインストールします。

```
$ sudo apt install -y git-buildpackage pbuilder piuparts dput
```

unstableに更新するのであれば、イントールされるパッケージ数を気にする必要もとくにないので`--no-install-recommends`を指定しなくてもよいでしょう。

残りの構築手順については、第6章と同じです。そちらを参照してください。

## 本章のまとめ

本章では、ConoHaでunstableのパッケージ開発環境を構築する手順を説明しました。

<section class="summary">

* VPSならunstable環境を維持するのも楽なので普段bookwormを使っている人にもおすすめ
* ConoHaならイメージ保存という手が使えるのでコストをおさえることも可能

</section>

dockerベースでパッケージをビルドするようにしたいなら、次のいずれかの章を参照するとよいでしょう。

* 実践 whalebuilderを使ってビルドする方法
* 実践 debockerを使ってビルドする方法

