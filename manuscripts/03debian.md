# Debianプロジェクトを構成する人々

本書はDebianコントリビューターになろうとしている人向けです。
そこで、Debianコントリビューターはどういう位置づけなのか、どんなことができるのかを解説します。

## パッケージングの観点からみた役割

Debianプロジェクトでは、いくつかの役割が設けられており、それぞれできることが違います。

* Debian Contributor (Debianコントリビューター)
* Debian Maintainer (Debianメンテナー)
* Debian Developer (Debian開発者)

では、それぞれの役割はどのように違うのでしょうか。

### Debian Contributor (Debianコントリビューター)

本書の対象となる役割です。

パッケージをDebian公式アーカイブに直接アップロードする権限はありません。

しかし、パッケージをメンテナンスできます。
これはどうしてかというと、パッケージをスポンサー(Debian開発者)にお願いしてアップロードしてもらえばよいからです。
パッケージをDebian公式アーカイブへとアップロードする権限こそなくても、パッケージのソースコードをメンテナンスしているリポジトリへのコミット権限をもらっていたりするなど、パッケージのメンテナンスをしていくうえで非常に重要な役割を果たしていたりします。

継続的にメンテナンスする場合はスポンサーにお願いする手間を省くことができるので、後述するDebianメンテナーになることをおすすめします。

### Debian Maintainer (Debianメンテナー)

特定のパッケージに関して、Debian公式アーカイブへとパッケージをアップロードする権限を持っています。

誰がどのパッケージのアップロードをできるかは、dm.txt<span class="footnote">https://ftp-master.debian.org/dm.txt</span>として公開されています。

```sh:dm.txtの内容(一部抜粋)
Fingerprint: 8FB9C5D6AE7BA82B7BB1D887C51EA7006E6BDC0D
Uid:   Taowa <taowa>
Allow: bombadillo (E44E9EA54B8E256AFB7349D3EC9D370872BC7A8C),
 dino-im (43DF758C18C7545A3E1A4F0A88237A6A53AB1B2E),
 golang-github-bndr-gotabulate (43DF758C18C7545A3E1A4F0A88237A6A53AB1B2E),
 ...
```

dm.txtの書式では、DebianメンテナーをGPG鍵のFingerprintで特定し、`Allow:`フィールドで、アップロード可能なパッケージをリストアップする方式になっています。

どうやったらDebian Maintainerになれるのかについては、「Debian Maintainerになるには」<span class="footnote">https://www.clear-code.com/blog/2018/8/30.html</span>という記事を公開しているので、参考にしてみてください。

### Debian Developer (Debian開発者)

Debianプロジェクトのメンバーとは、Debian開発者のことを指します。
Debian開発者は、仕組みの上ではどんなパッケージでもアップロードできます。

ただし、ほとんどの場合、それぞれのパッケージには定期的にメンテナンスしてくれているDebian開発者(やDebianメンテナー)がいます。
そのため、実際になんでもかんでもアップロードすることはありません。

また、Debian開発者であっても、新規パッケージを無審査でアップロードすることはできません。
これは、パッケージの審査担当の人がいて、パッケージの体裁や、ライセンスに適合しているかなどを厳しくチェックする体制がとられているからです。

なお、Debian開発者であっても、なかにはパッケージをアップロードする権限をもたないDebian開発者となっていることもあります。
Debian開発者になる申請をするときに、アップロードする権限つきかそうでないかを選べるからです。
なにもパッケージングすることだけがDebian開発者の役割ではありません。たとえば、コミュニティ活動やドキュメンテーションといったフィールドで活躍しているDebian開発者もいます。

Debian開発者になると、Debianプロジェクトの意思決定に関わることができるようになります。
具体的には、Debianプロジェクトリーダー選挙への投票であったり、GR(General Resolution)と呼ばれる議題へ投票する権利があります。
Debian Maintainerへパッケージをアップロードする許可を与えたりすることもできます。
また新規パッケージのスポンサーなどもします。

どうやったらDebian開発者になれるのかについては、「Debian Developerになるには」<span class="footnote">https://www.clear-code.com/blog/2020/9/28.html</span>という記事を公開しているので、参考にしてみてください。

<section class="column">

<h4>Debian Developerになるには</h4>

Debian開発者になるには、あなたが信頼に値する人物であることを示す必要があります。
以前はGPG鍵に既存のDebian開発者の複数の署名が必須とされていました。
しかし、昨今のコロナウィルスの流行などに代表されるように、対面でGPG鍵の交換が難しい状況を我々は経験しました。
そのため、GPG鍵の署名が今では必須ではなくなっています。コミュニティでのふるまいが信頼に足りうるかが評価されるようになっています。
とはいえ、Debianの理念やパッケージングに関する知識なども問われるプロセスがあることは変わりなく、それらをパスしなければなれません。
このプロセスのために、「Debian New Members」<span class="footnote">https://nm.debian.org/</span>という専用のサイトが用意されています。

</section>

---

<br>
