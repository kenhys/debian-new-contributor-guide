---
class: practice
---

# 実践: whalebuilderを使ってビルドする方法

`docker`ベースでパッケージをビルドするツールの1つに`whalebuilder`<span class="footnote">https://www.uhoreg.ca/programming/debian/whalebuilder</span>があります。
bookwormでは`whalebuilder` 0.9がインストールできます。

ドキュメントは[README.md](https://vcs.uhoreg.ca/git/cgit/whalebuilder/plain/README.md)<span class="footnote">https://vcs.uhoreg.ca/git/cgit/whalebuilder/plain/README.md</span>です。
`whalebuilder`をインストールすると、同じものが*/usr/share/doc*配下<span class="footnote">/usr/share/doc/whalebuilder/README.md.gz</span>にインストールされます。

<section class="warning">
第6章で説明したように、Debian bookwormでunstableのパッケージを開発できるようにしているものとします。ホストがDebian unstableである場合には、バージョンの差異により実行結果が異なることがあります。
</section>

## whalebuilderを使うメリット

whalebuilderを使うメリットは次のことがあげられます。

* bookwormに0.9があるので、`apt`で簡単に導入できる
* `docker`ベースなので、パッケージをビルドするのにホストのroot権限が必要ない
* 一度ビルドしたら依存関係をインストール済みのイメージを使ってビルドを繰り返せる(時間短縮)
* パッケージビルド用のイメージがあらかじめ公開されている

## whalebuilderを使うデメリット

* `gbp-buildpackage`との連携にひと工夫必要
* ビルド結果を任意のディレクトリにおけない
* `whalebuilder`ユーザーが追加されているイメージでないと使えない
* `whalebuilder`のベースイメージはそれほど頻繁に更新されてはいない

## インストール方法

```cmd
$ sudo apt install -y whalebuilder
```


## 使い方

あらかじめ、`docker`コマンドが使える状態になっているものとします。
実際に`whalebuilder`を使ってパッケージをビルドするには次のようにします。
サンプルとして、`hello`パッケージをビルドしてみましょう。

あらかじめ、`whalebuilder`向けのunstableのイメージがあるのでそれを使います。

```cmd
$ apt source hello
$ whalebuilder build hello_2.10-2.dsc 
```

パッケージのビルドが終わると、debパッケージは*~/.cache/whalebuilder*以下に作成されます。

```
$ tree ~/.cache/whalebuilder/
/home/debian/.cache/whalebuilder/
└── hello_2.10-2
    ├── hello_2.10-2_amd64.build
    ├── hello_2.10-2_amd64.buildinfo
    ├── hello_2.10-2_amd64.changes
    ├── hello_2.10-2_amd64.deb
    ├── hello_2.10-2.debian.tar.xz
    ├── hello_2.10-2.dsc
    ├── hello_2.10.orig.tar.gz
    └── hello-dbgsym_2.10-2_amd64.deb
```

この時点で、dockerイメージは次のものが保持されます。

```
$ docker images
REPOSITORY TAG IMAGE ID CREATED SIZE
whalebuilder_build/hello 2.10-2 5b7bbd6f15d0 17 seconds ago   898MB
whalebuilder/debian      sid    83d8da965285 2 months ago     779MB
```

`whalebuilder/debian:sid`と`whalebuilder_build/hello:2.10-2` という2つのイメージが作られていることがわかります。

`whalebuilder_build/hello:2.10-2`は`whalebuilder/debian:sid`をベースに作られたイメージです。
依存関係などもすでにインストールされているので、パッケージングの際に何度もビルドを試行錯誤するような場合には便利です。


## git-buildpackageと連携するには

`gbp buildpackage`で`pbuilder`ではなく、`whalebuilder`を使うには、*~/.gbp.conf*の`prebuild`、`builder`を次のように変更します。

```
[DEFAULT]
prebuild = debuild -S -us -uc -d
builder = whalebuilder build $GBP_BUILD_DIR/../*.dsc
```

上記の設定で次のように動作します。

* `prebuild`で.dscファイルを*../build-area*に作成する
* `builder`で`whalebuilder`を使ってパッケージをビルドします。.dscファイルのパス指定には`$GBP_BUILD_DIR`からの相対パスを指定しています。`$GBP_BUILD_DIR`は*../build-area/(パッケージ)-(バージョン)*を指すので、*../build-area/*にある.dscを使ってビルドすることになります。


設定ファイルのサンプルは本書のリポジトリから入手 <span class="footnote">https://salsa.debian.org/kenhys/debian-new-contributor-guide/-/blob/master/examples/gbp/whalebuilder.conf</span>できます。

---

## 本章のまとめ

本章では、パッケージのビルドに`whalebuilder`を使う方法を説明しました。

<section class="summary">

* `whalebuilder`を使うと`docker`ベースでパッケージをビルドできる
* `whalebuilder`の成果物は*$HOME/.cache/whalebuilder*配下にできる
* *~/.gbp.conf*の設定を切り替えるだけで、`gbp`を使いつつ、パッケージのビルドを`docker`ベースの`whalebuilder`に切り替えられる

</section>

`lintian`も`docker`ベースにしたい場合には、「実践:lintianをdockerで実行する方法」を参照してください。
