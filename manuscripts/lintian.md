---
class: practice
---

# 実践: lintianをdocker経由で実行する方法

<!--
前章までに、`docker`ベースでパッケージをビルドしたい場合のために、`whalebuilder`と`debocker`を説明しました。
ただし、`whalebuilder`の場合、`lintian`については、インストールして使う前提になっていました。
//-->

前章までは、`lintian`は`pbuilder`のフックで自動実行されるため、ホストにインストールしない前提で説明していました。
本章では、これを`docker`ベースにする方法を説明します。

* `lintian`のイメージを作成する
* `lintian`のイメージを使ってみる
* `git-buildpackage`と連携するには

<!--
* `whalebuilder`と連携するには
* `debocker`と連携するには
//-->

## lintianのイメージを作成する

`docker`ベースで`lintian`を実行するには、そのためのイメージが必要です。
`unstable`をベースにイメージを作成します。
まずは`Dockefile`を用意します。

```cmd:Dockerfileの内容
FROM debian:sid

RUN apt update && apt install -y lintian

ENTRYPOINT ["lintian", "-EviIL", "+pedantic"]
CMD ["/debian/hello.changes"]
```

ホストの作業ディレクトリを`/debian`にマウントして、指定された`*.changes`を`lintian`にかけるという方式です。

次のようにして、`debian:lintian`イメージをビルドします。

```
$ docker build . -t debain:lintian
```

## lintianのイメージを使ってみる

イメージがビルドできたので実際に使ってみましょう。
docker経由で実行するには次のコマンドを実行します。

```
~/work $ docker --rm -v $PWD:/debian debian:lintian build-area/hello_2.10-2_amd64.changes
```

## git-buildpackageと連携するには

gbp buildpackageで`lintian`を`docker`ベースで実行するには、*~/.gbp.conf*の`postbuild`を次のように変更します。

```
postbuild = docker --rm -v $GBP_BUILD_DIR/..:/debian debian:lintian \
  /debian/$(basename $GBP_CHANGES_FILE) 2>&1 \
  | tee ../lintian-full.log && \grep -E "^(E:|I:|W:|P:)" ../lintian-full.log \
  | tee ../lintian-simple.log && echo "Lintian DONE"
```

設定ファイルのサンプルは本書のリポジトリから入手 <span class="footnote">https://salsa.debian.org/kenhys/debian-new-contributor-guide/-/blob/master/examples/gbp/pbuilder-lintian.conf</span>できます。

## whalebuilderと連携するには

ビルダーが`whalebulder`のときに、`lintian`を`docker`ベースで実行するには、*~/.gbp.conf*の`postbuild`を次のように変更します。

```
postbuild = docker --rm \
  -v $HOME/.cache/whalebuilder/$(basename $GBP_CHANGES_FILE "_amd64.changes"):/debian \
  debian:lintian \
  /debian/$(basename $GBP_CHANGES_FILE) 2>&1 \
  | tee ../lintian-full.log && \
  \grep -E "^(E:|I:|W:|P:)" ../lintian-full.log \
  | tee ../lintian-simple.log && echo "Lintian DONE"
```

設定ファイルのサンプルは本書のリポジトリから入手 <span class="footnote">https://salsa.debian.org/kenhys/debian-new-contributor-guide/-/blob/master/examples/gbp/whalebuilder-lintian.conf</span>できます。

## 本章のまとめ

本章では、`lintian`を`docker`経由で実行する方法を紹介しました。
最新のlintianをbullseyeでも利用したい場合に、便利でしょう。

ホストに開発関連のパッケージをあまりインストールしたくない場合にも役立ちます。
`docker`ベースでパッケージをビルドしたいなら、次の章も参照してください。

* 実践 `whalebuilder`を使ってビルドする方法
* 実践 `debocker`を使ってビルドする方法

---

<br>
