---
class: practice
---

# 実践: Go言語のパッケージングをしてみよう

この章では、Go言語のパッケージを作成する方法について説明します。

Go言語の場合、Debian Go Packaging Teamというものがあります。
公式サイトは https://go-team.pages.debian.net/ です。

salsaにはgo-team<span class="footnote">https://salsa.debian.org/go-team</span>というチームのプロジェクトが存在しています。
各種パッケージはチームのプロジェクト配下の`packages`<span class="footnote"> https://salsa.debian.org/go-team/packages </span>以下でメンテナンスされています。

Goのパッケージでは、`dh-make-golang`を使うのがお作法となっています。
`dh-make-golang`を使ったパッケージングに関するドキュメントとしては、次の2つを参照するとよいでしょう。

* [Introduction to dh-make-golang](https://people.debian.org/~stapelberg/2015/07/27/dh-make-golang.html) <span class="footnote">https://people.debian.org/~stapelberg/2015/07/27/dh-make-golang.html</span>
* [Debian Go Packaging](https://go-team.pages.debian.net/packaging.html) <span class="footnote">https://go-team.pages.debian.net/packaging.html</span> 

Goのパッケージを作成するには以下の流れにしたがうとよいでしょう。

* 開発に必要なパッケージをインストールする(初回のみ)
* `dh-make-golang`を使ってsalsaにプロジェクトを作成する
* `dh-make-golang`を使ってパッケージの雛形を作成する
* 作成したテンプレートをもとにITPの登録をする
* パッケージの雛形を整える
* mentors.debian.netにアップロードする
* スポンサーを探してパッケージをアップロードしてもらう

本章では、例として、`lithammer/fuzzysearch`<span class="footnote">https://github.com/lithammer/fuzzysearch</span>を題材にパッケージングする方法を説明します。

<section class="warning">
第6章で説明したように、Debian bullseyeでunstableのパッケージを開発できるようにしているものとします。ホストがDebian unstableである場合には、バージョンの差異により実行結果が異なることがあります。
</section>

## 開発に必要なパッケージをインストールする(初回のみ)

`dh-make-golang`は明示的にインストールする必要があります。

```cmd
$ sudo apt install -y dh-make-golang dh-golang
```

Go言語でパッケージングするためのモジュールを提供してくれる`dh-golang`も一緒にインストールしておきます。<span class="footnote">`dh-golang`のインストールを忘れると、パッケージをビルドできません。</span>

## dh-make-golangを使ってsalsaにプロジェクトを作成する

salsaにプロジェクトを作成する場合には、次のコマンドを実行します。

```cmd
$ dh-make-golang create-salsa-project golang-github-lithammer-fuzzysearch
```

GitHubにパッケージング対象のソフトウェアが置いてある場合は、*golang-github-* というプレフィクスをつけたものをプロジェクト名とすることになっています。

したがって、https://github.com/lithammer/fuzzysearch の場合は、`golang-github-lithammer-fuzzysearch`がプロジェクト名です。

コマンドが正常に終了すると、次の場所にプロジェクトが作成されます。

https://salsa.debian.org/go-team/packages/golang-github-lithammer-fuzzysearch

ただし、`dh-make-golang create-salsa-project`はDebianコントリビューターの権限では実行できません。
お近くのDebian開発者に相談するとよいでしょう。
いったんプロジェクトがsalsaに作成できたら、該当プロジェクトのリポジトリへのコミット権限をもらってください。

## dh-make-golangを使ってパッケージの雛形を作成する

`dh-make-golang`を次のように実行すると、雛形を作成できます。

```cmd
~/work $ dh-make-golang make github.com/lithammer/fuzzysearch
```

すると、`golang-github-lithammer-fuzzysearch`ディレクトリが作成されます。

ディレクトリの構成内容は次の通りです。

```cmd:生成された雛形のディレクトリ内容
~/work/golang-github-lithammer-fuzzysearch $ tree
.
├── LICENSE
├── README.md
├── debian
│   ├── changelog
│   ├── control
│   ├── copyright
│   ├── gbp.conf
│   ├── gitlab-ci.yml
│   ├── rules
│   ├── source
│   │   └── format
│   ├── upstream
│   │   └── metadata
│   └── watch
├── fuzzy
│   ├── fuzzy.go
│   ├── fuzzy_test.go
│   ├── levenshtein.go
│   └── levenshtein_test.go
├── go.mod
└── go.sum
```

雛形には、debian/*ディレクトリも含まれていることがわかります。
同時にリポジトリのremoteも設定済みとなります。

```cmd
~/work/golang-github-lithammer-fuzzysearch $ git remote -v
github  https://github.com/lithammer/fuzzysearch (fetch)
github  https://github.com/lithammer/fuzzysearch (push)
origin  git@salsa.debian.org:go-team/packages/ golang-github-lithammer-fuzzysearch.git (fetch)
origin  git@salsa.debian.org:go-team/packages/ golang-github-lithammer-fuzzysearch.git (push)
```

また、ブランチも同時に作成されます。

```cmd:dh-make-golangが作成するブランチ
~/work/golang-github-lithammer-fuzzysearch $ git branch -a
* debian/sid
  pristine-tar
  upstream
  remotes/github/master
```

パッケージング作業では、`debian/sid`ブランチを更新していきます。
これは、生成された*debian/gbp.conf*に次のようなブランチの設定があるためです。

```text:debian/gbp.conf
[DEFAULT]
debian-branch = debian/sid
dist = DEP14
```

## 作成したテンプレートをもとにITPの登録をする

`dh-make-golang`を実行したときに、同時にITPの雛形も一緒に作成してくれます。

ITPの雛形は、*itp-golang-github-lithammer-fuzzysearch.txt*です。
*golang-github-lithammer-fuzzysearch*ディレクトリと同一の階層に作成されます。
抜粋すると、次のようになっているはずです。

```text:itp-golang-github-lithammer-fuzzysearch.txtの内容(抜粋)
From: "Kentaro Hayashi" <kenhys@xdump.org>
To: submit@bugs.debian.org
Subject: ITP: golang-github-lithammer-fuzzysearch -- :pig: Tiny and fast fuzzy \
  search in Go
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
X-Debbugs-CC: debian-devel@lists.debian.org, debian-go@lists.debian.org

Package: wnpp
Severity: wishlist
Owner: Kentaro Hayashi <kenhys@xdump.org>

* Package name    : golang-github-lithammer-fuzzysearch
  Version         : 1.1.3-1
  Upstream Author : Peter Lithammer
* URL             : https://github.com/lithammer/fuzzysearch
* License         : Expat
  Programming Lang: Go
  Description     : :pig: Tiny and fast fuzzy search in Go

 ...
 
```

ある程度リポジトリから自動的に内容を埋めてくれるので、`License:`や`Description:`を適切な内容に更新しておきます。

修正が終わったら、`Package: wnpp`以降の内容をメールの本文にして、`submit@bugs.debian.org`宛にITPのメールを送信します。
このあたりは第5章で説明したとおりです。

## パッケージの雛形を整える

パッケージの雛形を整えるには、第7章や第8章で説明したパッケージングのお作法にならって作業します。

* debian配下をコミットする
* パッケージの依存関係を修正する
* パッケージがビルドできることを確認する
* `lintian`に指摘された内容を修正する
* mentors.debian.netにアップロードする
* スポンサーを探してパッケージをアップロードしてもらう

### debian配下をコミットする

debian配下のディレクトリはコミットされていないので、まずはコミットします。
その後、次のコマンドを実行するとパッケージのビルドを実行できるようになります。

```cmd
~/work/golang-github-lithammer-fuzzysearch $ gbp buildpackage \
  --git-prebuild="dch --release ''"
```

`--git-prebuild="dch --release ''"`は一時的に*debian/changelog*のリリースをバッチ更新するために指定しています。
これは*debian/changelog*のリリースが`UNRELEASED`のままではビルドできないためです。

つまり、次のようなことを一時的に実施します。

```text
diff --git a/debian/changelog b/debian/changelog
index 6a51d93..e52ec5a 100644
--- a/debian/changelog
+++ b/debian/changelog
@@ -1,5 +1,5 @@
-golang-github-lithammer-fuzzysearch (1.1.3-1) UNRELEASED; urgency=medium
+golang-github-lithammer-fuzzysearch (1.1.3-1) unstable; urgency=medium
 
   * Initial release (Closes: TODO)
...
```

上記のリリースの変更は、パッケージの体裁が整ったらコミットするとよいでしょう。
Goのパッケージングでは、リリース直前までは`UNRELEASED`のままにしておくルールがあるようです。<span class="footnote">`--git-prebuild`を指定するのは暫定処置なので、はやめにリリースできるとよいですね。</span>

ただし、依存関係に誤りがあるので、このままだと途中で失敗します。

```text
The following packages have unmet dependencies:
pbuilder-satisfydepends-dummy : Depends: golang-x-text-dev which is a virtual
package and is not provided by any available package

Unable to resolve dependencies!  Giving up...
```

### パッケージの依存関係を修正する

正しい依存先のパッケージ名は`golang-golang-x-text-dev`です。
`Build-Depends:`を一行修正してコミットしておけば、パッケージがビルドできるようになります。

```text
diff --git a/debian/control b/debian/control
index 2d8672d..3e9a380 100644
--- a/debian/control
+++ b/debian/control
@@ -7,7 +7,7 @@ Priority: optional
 Build-Depends: debhelper-compat (= 13),
                dh-golang,
                golang-any,
-               golang-x-text-dev
+               golang-golang-x-text-dev
 Standards-Version: 4.6.0
```

### パッケージがビルドできることを確認する

パッケージの依存関係を修正したので、再度ビルドしてみましょう。
問題なくビルドが通ったら、親の階層に*build-area*ディレクトリができています。
そのディレクトリにビルドできたパッケージが配置されます。

```cmd
~/work $ tree build-area
build-area
├─ golang-github-lithammer-fuzzysearch-dev_1.1.3-1_all.deb
├─ golang-github-lithammer-fuzzysearch_1.1.3-1.debian.tar.xz
├─ golang-github-lithammer-fuzzysearch_1.1.3-1.dsc
├─ golang-github-lithammer-fuzzysearch_1.1.3-1_amd64.build
├─ golang-github-lithammer-fuzzysearch_1.1.3-1_amd64.buildinfo
├─ golang-github-lithammer-fuzzysearch_1.1.3-1_amd64.changes
├─ golang-github-lithammer-fuzzysearch_1.1.3-1_source.changes
└─ golang-github-lithammer-fuzzysearch_1.1.3.orig.tar.gz
```

あとは、`lintian`の結果をチェックして、地道に問題点を潰していけばよいです。

### lintianに指摘された内容を修正する

ビルドログに含まれる`lintian`の内容を確認してみましょう。

```text:lintianのチェック結果
W: golang-github-lithammer-fuzzysearch-dev: 
  extended-description-contains-empty-paragraph
W: golang-github-lithammer-fuzzysearch-dev: initial-upload-closes-no-bugs
I: golang-github-lithammer-fuzzysearch-dev: 
  capitalization-error-in-description russian Russian
I: golang-github-lithammer-fuzzysearch source: 
  out-of-date-standards-version 4.5.0 (released 2020-01-20) (current is 4.6.0.1)
X: golang-github-lithammer-fuzzysearch source: 
  debian-watch-does-not-check-gpg-signature
```

* W: golang-github-lithammer-fuzzysearch-dev: extended-description-contains-empty-paragraph

*debian/control*の`golang-github-lithammer-fuzzysearch-dev`パッケージの説明に関して、空行があることを指摘されています。

例えば、`Description:`フィールドに次のように空行を示す`.`が複数行続けて存在している箇所が該当します。

```
Description: Short description
 Extended description
 .
 .
 End of Extended description.
```

対処は、`.`が連続しないように削除することです。

* W: golang-github-lithammer-fuzzysearch-dev: initial-upload-closes-no-bugs

ITPをクローズするための対象バグ番号を記載していないことを指摘されています。
対象のバグ番号を*debian/changelog*に記載します。

```
diff --git a/debian/changelog b/debian/changelog
index 6a51d93..53b89f8 100644
--- a/debian/changelog
+++ b/debian/changelog
@@ -1,5 +1,5 @@
 golang-github-lithammer-fuzzysearch (1.1.3-1) UNRELEASED; \
 urgency=medium
 
-  * Initial release (Closes: TODO)
+  * Initial release (Closes: #992838)
```

* I: golang-github-lithammer-fuzzysearch-dev: capitalization-error-in-description russian Russian

`debian/control`の`Description:`で単純に大文字で始まっていないことを指摘されています。文脈に応じて`russian`を`Russian`に修正します。

* I: golang-github-lithammer-fuzzysearch source: out-of-date-standards-version 4.5.0 (released 2020-01-20) (current is 4.6.0.1)

*debian/control*の`Standards-Version:`フィールドが古いことを指摘されています。
2024年11月時点の最新版は4.7.0なので次のように修正します。

```text
diff --git a/debian/control b/debian/control
index 3e9a380..4d4d5a6 100644
--- a/debian/control
+++ b/debian/control
@@ -8,7 +8,7 @@ Build-Depends: debhelper-compat (= 13),
                dh-golang,
                golang-any,
                golang-golang-x-text-dev
-Standards-Version: 4.5.0
+Standards-Version: 4.7.0
```

`Standards-Version`を更新するときは、最新のポリシーに追従できているか、[Upgrading checklist](https://www.debian.org/doc/debian-policy/upgrading-checklist.html)<span class="footnote">https://www.debian.org/doc/debian-policy/upgrading-checklist.html</span>を確認してください。

* X: golang-github-lithammer-fuzzysearch source: debian-watch-does-not-check-gpg-signature

これはアップストリームがGPGで署名してあることを前提に、*debian/watch*でそれをチェックしていないと指摘されています。
しかし、この場合はそもそもアップストリームがアーカイブにGPGで署名をしていないので、ひとまず気にする必要はありません。

あとは、*debian/copyright*が十分記述されているかなどが重要ですが、ここでは詳細を割愛します。

mentors.debian.netにアップロードについては、第10章で説明しているのでそちらを参照してください。
スポンサーを探してパッケージをアップロードしてもらう件については、第11章で説明しているのでそちらを参照してください。

## 本章のまとめ

本章では、`lithammer/fuzzysearch`を題材にパッケージングする方法を説明しました。

<section class="summary">

* Go言語のパッケージはツールも整備されているので、比較的とっつきやすい

</section>

もしまだパッケージ化されていないものがあれば、ぜひ挑戦してみてください。

