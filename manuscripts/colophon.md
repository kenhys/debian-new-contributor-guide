## あとがき

「Debian新コントリビューターガイド」はいかがでしたでしょうか。
可能な限り技術的な誤り等がないように留意していますが、もし見つけたらメール<span class="footnote">`kenhys+debian-new-contributor-guide@gmail.com`</span>を送るか、salsaのissue<span class="footnote">https://salsa.debian.org/kenhys/debian-new-contributor-guide/-/issues/new</span>としてフィードバックしてもらえると助かります。

初版では、技術書典12の「後から印刷」<span class="footnote">技術書典12終了後に印刷数を決めて刷ることができるしくみ。大量の在庫を抱えるリスクを減らせるのが参加サークルのメリット。一方で自分でバックアップ印刷所を手配したときのような早期割引等は使えない。小部数の場合は悩ましい。</span>の仕様でA5だとページ数の上限が160pまで<span class="footnote">160pを超える場合は要相談。</span>のため、審査直前でいくつか実践編の章を落としました。

しかし、「後から印刷」であってもB5であればページ数の上限が128pです。
版面が広くなるので、なんとか収録可能です。
ということで、第二版ではA5からB5に変更しました。
これにより省略した次の章を復活させることができました。

* 実践：`whalebuilder`を使ってみよう
* 実践：`debocker`を使ってみよう
* 実践：`lintian`を`docker`で実行する方法

ただし、やりたいことをやるためには、当初想定していたより印刷費用がかさんだり、データを作り直す必要がでてきたりと代償を払うことになりました。何事もご利用は計画的に。

---

## 著者紹介

![著者近影(Illustration by 朝倉世界一)](../images/kenhys.png){width=30%}

TrackPoint愛好家。
ソフトドーム派に所属。
ひよこDebian Developer。
ソフトウェアをもっと便利にするためにささやかながら活動中。
気分転換には珈琲を。
わさビーフは神の食べ物。
「Make unstable life comfortable」をスローガンにひっそり暮しています。
既刊に「Mozilla Firefoxを企業(団体)向けにカスタマイズする技術」初出:技術書典5、
「Mozilla Firefoxを企業(団体)向けカスタマイズ入門(ESR102対応)」初出：技術書典13、
「Ultimate Hacking Keyboard 60入門」初出：技術書典14、
「BIMI向けSVG Tiny PSをテキストエディタで作成するための本」初出：技術書典15などがある。

---

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<section id="colophon" role="doc-colophon">

|        |                                |
|------------|------------------------------------|
| タイトル   | Debian新コントリビューターガイド   |
| 発行年月日 | 2022年1月22日 初版発行 |
|  | 2022年1月23日 第二版発行 |
|  | 2022年2月27日 第三版発行 |
|  | 2024年11月2日 第四版発行 |
| 発行       | 氷鼬組                             |
| 著者       | © 2021-2024, 林健太郎(@kenhys)       |

</section>

