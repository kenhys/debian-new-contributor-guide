# さらなるヒントを求めて

本章では、今後も継続的にDebianコミュニティーで活動していくにあたって、参考となりそうな資料等へのリンクなどを紹介します。

* Debianでのパッケージングの流れを把握するための記事
* パッケージングのお作法に関するドキュメント
* 相談先としておすすめのメーリングリスト
* Goのパッケージングに関連したドキュメント
* Debian開発者をめざすための記事

## Debianでのパッケージングの流れを把握するための記事

* [Debianでパッケージをリリースできるようにしたい - WNPPへのバグ登録](https://www.clear-code.com/blog/2014/3/7.html) <span class="footnote">https://www.clear-code.com/blog/2014/3/7.html</span> 

第5章で説明した、パッケージ化する宣言(ITP)をするやりかたについての記事(2014年3月)です。

Debianでよく使われる略語についても[略語の解説](https://www.debian.or.jp/community/devel/abbreviation.html)として <span class="footnote">https://www.debian.or.jp/community/devel/abbreviation.html</span>言及しているので参考になることでしょう。

* [Debianでパッケージをリリースできるようにしたい - よりDebianらしく](https://www.clear-code.com/blog/2014/4/3.html)<span class="footnote">https://www.clear-code.com/blog/2014/4/3.html</span>

第8章で説明した、`lintian`を使ってパッケージの直したほうがよいところを見つけるための記事(2014年4月)です。

* [Debianでパッケージをリリースできるようにしたい - mentors.debian.netの使いかた](https://www.clear-code.com/blog/2014/7/2.html) <span class="footnote">https://www.clear-code.com/blog/2014/7/2.html</span>

第10章で説明した、mentors.debian.netの使い方の記事(2014年7月)です。

* [Debianでパッケージをリリースできるようにしたい - そしてDebianへ](https://www.clear-code.com/blog/2014/10/31.html) <span class="footnote">https://www.clear-code.com/blog/2014/10/31.html</span>

スポンサーにアップロードしてもらったあとDebianに入るまでの流れを説明した記事(2014年10月)です。

* [Debian向けにパッケージングするメリットとは](https://www.clear-code.com/blog/2016/7/20.html)

Debianにパッケージがあるとどう嬉しいのかを具体的な例をもとに説明した記事(2016年7月)です。
Debian向けにパッケージングをする過程で、ライセンス上の問題があったりしたのを解決した事例を紹介しています。

* [Ubuntuでdebパッケージのテストをするには](https://www.clear-code.com/blog/2014/12/1.html)

パッケージのインストールテストを行う`piuparts`について知るための記事(2014年12月)です。
  
* [Ubuntuでdebパッケージのお手軽クリーンルーム（chroot）ビルド環境を構築するには](https://www.clear-code.com/blog/2014/11/21.html)

`pbuilder`以外にも、いくつか代替のbuilderがあります。これは`cowbuilder`について知る記事(2014年11月)です。


## パッケージングのお作法に関するドキュメント

* [Debian Policy Manual](https://www.debian.org/doc/debian-policy/) <span class="footnote">https://www.debian.org/doc/debian-policy/</span>

`lintian`でエラーになったときなどに参照します。
[アップグレードチェックリスト](https://www.debian.org/doc/debian-policy/upgrading-checklist.html) <span class="footnote">https://www.debian.org/doc/debian-policy/upgrading-checklist.html</span>は最新のポリシーに準拠できているか確認するのに便利です。

* [Machine-readable debian/copyright file](https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/)

*debian/copyright*の書き方に困ったら参照します。

* [Debian Developers Reference](https://www.debian.org/doc/manuals/developers-reference/)

Debian開発者リファレンスです。[6章のパッケージ化のベストプラクティス](https://www.debian.org/doc/manuals/developers-reference/best-pkging-practices.ja.html)が参考になるはずです。

## 相談先としておすすめのメーリングリスト

パッケージングに関して相談したいときに使えるチャンネルがいくつかあります。

* https://lists.debian.org/debian-mentors/ (英語)

パッケージング一般に関する相談をするときはこのメーリングリストを利用するとよいでしょう。
ただしやりとりは英語でする必要があります。

* https://lists.debian.org/debian-go/ (英語)

go teamに相談したいことがあるときはこのメーリングリストを利用します。

* https://lists.debian.or.jp/mailman/listinfo/debian-devel/

日本語でパッケージに限らず相談したいときはこのメーリングリストを利用します。
  
## Goのパッケージングに関連したドキュメント

* https://go-team.pages.debian.net/packaging.html

実践編でも紹介している、DebianプロジェクトでのGoのパッケージング方法について説明した記事です。

* https://tokyodebian-team.pages.debian.net/pdf2021/debianmeetingresume202105.pdf

Debian勉強会でDebianにおけるGoについて発表されたことがありました。そのときの勉強会の資料(2021年5月)です。主にGoの使い方ですが、`dh-make-golang`にも少し言及があります。

* https://people.debian.org/~stapelberg/2015/07/27/dh-make-golang.html

実践編でも紹介している`dh-make-golang`の使い方を説明している記事(2015年7月)です。

## Debian開発者をめざすための記事

* [Debian Maintainerになるには](https://www.clear-code.com/blog/2018/8/30.html) <span class="footnote">https://www.clear-code.com/blog/2018/8/30.html</span> (2018年8月の記事)

* [Debian Developerになるには](https://www.clear-code.com/blog/2020/9/28.html) <span class="footnote">https://www.clear-code.com/blog/2020/9/28.html</span> (2020年9月の記事)

## 本章のまとめ

本章では、今後も継続的にDebianコミュニティーで活動していくにあたって、参考となりそうな資料等へリンクなどを紹介しました。

