# パッケージングをしてみよう

第5章でITPを実施した`libfreeaptx`を例にパッケージングのやりかたを説明します。

パッケージング対象のソフトウェアによって、パッケージングのお作法が異なります。
今回は、C言語のライブラリーのパッケージングを説明します。

<section class="warning">
本章では第6章で説明したホストに開発関連のパッケージをインストールしたものとして説明します。
</section>

本章では次のトピックを説明します。

* どこにリポジトリを作成すべきか?
* Gitでパッケージのソースコードを管理し始めるには
* debian/*の内容を整備する
* 修正内容をpushする

<section class="column">

<h4>どれくらいパッケージングのお作法が違うの?</h4>

Debianでは言語だったりテーマごとにパッケージングチームが存在し、チーム単位でメンテナンスされているものも多いです。
言語ごとにパッケージングで使うツールが違ったりします。

* [Ruby](https://salsa.debian.org/ruby-team)<span class="footnote">https://salsa.debian.org/ruby-team</span>`gem2deb`を使う
* [Go](https://salsa.debian.org/go-team)<span class="footnote">https://salsa.debian.org/go-team</span>`dh-make-golang`を使う
* [Python](https://salsa.debian.org/python-team)<span class="footnote">https://salsa.debian.org/python-team</span>`dh-python`を使う

本書では、Go言語の場合を実践編で解説しています。
</section>

## どこにリポジトリを作成すべきか?

最近では、DebianパッケージのソースコードはGitで管理されるようになってきました。
そのGitのリポジトリは[salsa](https://salsa.debian.org)<span class="footnote">https://salsa.debian.org</span>で管理されることが多いです。
salsaはDebianプロジェクトが運用しているGitLabのインスタンスです。
GitLabを使ったことがあれば、とくに違和感なく使えることでしょう。<span class="footnote">しかし、昨今ではGitHub全盛すぎて、GitLabを使ったことがなくて戸惑うケースもあるようです。</span>

salsaでは、チームでメンテナンスされているものは各チームのグループ配下に、
そうでないものはdebian<span class="footnote">https://salsa.debian.org/debian</span>配下でメンテナンスします。
debian/名前空間の配下はDebian開発者が既定でパッケージの修正に参加ができる権限があるので、パッケージを複数名でメンテナンスするのにおすすめです。

Debian Contributorでは、直接debian配下やチーム配下にリポジトリをつくることができません。
そのため、以下のいずれかの手段をとることになります。

* いったんどこかのGitホスティングサービスにリポジトリを作成して、それをsalsaにインポートしてもらう
* いったん個人のリポジトリをsalsaに作成して、ある程度パッケージングがすすんだ段階で移動する
* 最初からスポンサーをさがしてdebian/配下 or 適切なチーム配下にリポジトリを作成してもらうように調整し、コミット権限をもらう。

いずれにせよ、salsaにリポジトリを置くなら、権限をもっているDebian開発者と相談することになります。

別解としては、salsaには置かない、という選択肢もあります。
ただ、Debian開発者と協力して作業しやすかったり、リポジトリがあちこちに分散せずにすむことから、salsaにリポジトリを作成してメンテナンスしていくことをおすすめします。

<section class="column">

<h4>Debian開発者と連絡をとるには?</h4>

Debian勉強会<span class="footnote">https://tokyodebian-team.pages.debian.net/</span>が毎月1回の頻度で定期的に開催されています。
オンラインでの開催なので、時間の都合さえあえば参加しやすいでしょう。Debian開発者も参加していることがあるので、相談しやすいはずです。
最近では、オフラインイベントとして開催も試みられています。
オープンソースカンファレンスへ出展していることもあるので、出展ブースで相談するのもよいでしょう。

もし、Debian勉強会への参加が難しいようなら、
Debian JP Developers メーリングリスト<span class="footnote">https://lists.debian.or.jp/mailman/listinfo/debian-devel/</span>があります。
そちらで相談してみてください。
</section>


## Gitでパッケージのソースコードを管理し始めるには

これまでの章で言及した`gbp`コマンドを使うと、簡単に管理を行えるようになります。

今回パッケージング例を解説するのは`libfreeaptx`です。
これは、aptXコーデックをサポートするのに必要なライブラリーです。
アップストリームはGitHubにソースコードが<span class="footnote">https://github.com/iamthehorker/libfreeaptx</span>あり、2024年11月時点の最新版は0.1.1です。

以降の説明では、作業ディレクトリーを *~/work/*とします。

### リポジトリを初期化する

作業用のディレクトリを作成して、次のようにしてリポジトリの初期化を行います。

```
~/work $ mkdir libfreeaptx
~/work $ cd libfreeaptx
~/work/libfreeaptx $ git init
~/work/libfreeaptx $ git remote add origin git@salsa.debian.org:debian/libfreeaptx.git
```

### ソースコードをインポートする

リポジトリの初期化ができたら、次はあらかじめダウンロードしておいたアーカイブをインポートします。
インポートするには、`gbp`コマンドの`import-orig`サブコマンドを指定します。

```
~/work/libfreeaptx $ curl -L -o ../libfreeaptx-0.1.1.tar.gz \
 https://github.com/iamthehorker/libfreeaptx/archive/refs/tags/0.1.1.tar.gz
~/work/libfreeaptx $ gbp import-orig ../libfreeaptx-0.1.1.tar.gz
What will be the source package name? [libfreeaptx] 
What is the upstream version? [0.1.1] 
gbp:info: ../libfreeaptx_0.1.1.orig.tar.gz already exists,
moving to ../libfreeaptx_0.1.1.orig.tar.gz.1636195197
gbp:info: Importing '../libfreeaptx-0.1.1.gbp.tar.gz' to branch 'upstream' (filtering
 out ['*egg.info', '.bzr', '.hg','.hgtags','.svn', 'CVS', '*/debian/*', 'debian/*'])...
gbp:info: Source package is libfreeaptx
gbp:info: Upstream version is 0.1.1
gbp:info: Successfully imported version 0.1.1 of ../libfreeaptx_0.1.1.orig.tar.gz
```

途中ソースパッケージ名と、アップストリームのバージョンの確認がありますが、そのままでよいのでEnterを入力します。

すると、次のように3つのブランチが作られます。

```
~/work/libfreeaptx $ git branch -a
* debian/unstable
  pristine-tar
  upstream
```

`debian/unstable`はパッケージングのための*debian/*ディレクトリをメンテナンスするためのブランチです。
`pristine-tar`はアーカイブを生成しなおすときにつじつまを合わせるための特別なブランチです。最初のうちは気にしなくてよいです。
`upstream`はアップストリームの変更を取り込むためのブランチです。

パッケージング作業をすすめるときは、`debian/unstable`ブランチ<span class="footnote">パッケージによってはmasterだったりdebian/sidだったりすることがあります。</span>で主に作業します。
`debian/unstable`ブランチの内容は次のようになっているはずです。

```
~/work/libfreeaptx $ tree .
.
├── .git
├── COPYING
├── Makefile
├── README
├── freeaptx.c
├── freeaptx.h
├── freeaptxdec.c
└── freeaptxenc.c
```

### テンプレートを作成する

`gbp import-orig`で最新のソースコードをとりこみましたが、パッケージングに必要なdebian/ディレクトリがありません。
テンプレートファイルは`dh_make`コマンドを使うと生成できます。

```
~/work/libfreeaptx $ dh_make --file ../libfreeaptx-0.1.1.tar.gz \
  --packagename libfreeaptx_0.1.1 --library --yes
Maintainer Name     : Debian User
Email-Address       : debian@example.jp
Date                : Sat, 06 Nov 2021 20:10:46 +0900
Package Name        : libfreeaptx
Version             : 0.1.1
License             : blank
Package Type        : library
Done. Please edit the files in the debian/ subdirectory now.

Make sure you edit debian/control and change the Package: lines from libfreeaptxBROKEN \
to something else, such as libfreeaptx1
```

今回パッケージングするのはライブラリーなので、`--library`を指定します。
`dh_make`は既定では展開済みのアーカイブディレクトリ(例えば、libfreeaptx-0.1.1)を前提としているので、
今回のようにリポジトリのディレクトリ名にバージョンを含めていない場合、`--packagename`に`libfreeaptx_0.1.1`を指定します。
このとき、パッケージとバージョンは`_`で区切ります。
`--yes` は確認を省略するために指定します。

`dh_make`でテンプレートを作成すると次のようなファイルが作成されます。

```
~/work/libfreeaptx $ tree debian
debian
├── README.Debian
├── README.source
├── changelog
├── control
├── copyright
├── libfreeaptx-dev.dirs
├── libfreeaptx-dev.install
├── libfreeaptx-docs.docs
├── libfreeaptx.cron.d.ex
├── libfreeaptx.doc-base.ex
├── libfreeaptx1.dirs
├── libfreeaptx1.install
├── manpage.1.ex
├── manpage.md.ex
├── manpage.sgml.ex
├── manpage.xml.ex
├── postinst.ex
├── postrm.ex
├── preinst.ex
├── prerm.ex
├── rules
├── salsa-ci.yml.ex
├── shlibs.local.ex
├── source
│   └── format
└── watch.ex
```

`.ex`という拡張子がついているものがテンプレートです。`.ex`という拡張子を削除して使います。
必要なければ該当ファイルを削除してかまいません。

今回の場合は次のファイルを削除してしまいましょう。

* *README.Debian* (特記事項がないので削除)
* *README.source* (特記事項がないので削除)
* *libfreeaptx-docs.docs* (README.DebianやREADME.sourceが不要なので削除)
* *libfreeaptx.cron.d.ex* (cron関係ないので削除)
* *libfreeaptx.doc-base.ex* (doc-base登録しないので削除)
* *manpage.1.ex* (マニュアルは別途作成するので削除)
* *manpage.md.ex* (マニュアルは別途作成するので削除)
* *manpage.sgml.ex* (マニュアルは別途作成するので削除)
* *manpage.xml.ex* (マニュアルは別途作成するので削除)
* *postinst.ex* (メンテナスクリプト不要なので削除)
* *postrm.ex* (メンテナスクリプト不要なので削除)
* *preinst.ex* (メンテナスクリプト不要なので削除)
* *prerm.ex* (メンテナスクリプト不要なので削除)
* *shlibs.local.ex* (あとで.symbolsを用意するので削除)

不要なファイルを削除していくと、次のようになります。

```
~/work/libfreeaptx $ tree debian
debian
├── changelog
├── control
├── copyright
├── freeaptxenc.md
├── libfreeaptx-dev.dirs
├── libfreeaptx-dev.install
├── libfreeaptx0.dirs
├── libfreeaptx0.install
├── rules
├── salsa-ci.yml
├── source
│   └── format
└── watch
```

これで、ひとまず必要なファイルの仕分けができました。

## debian/*の内容を整備する

ここまででテンプレートを整理しました。
それっぽい構成にはなっていますが、テンプレートのままなので修正が必要です。

具体的には次のファイルを修正する必要があります。

* *debian/changelog*
* *debian/control*
* *debian/copyright*
* *debian/freeaptxenc.md*
* *libfreeaptx-dev.dirs*
* *libfreeaptx-dev.install*
* *libfreeaptx0.dirs*
* *libfreeaptx0.install*
* *debian/rules*
* *debian/salsa-ci.yml*
* *debian/watch*

順番に説明していきます。

### debian/changelogを修正する

*debian/changelog*は変更点をまとめるためのファイルです。debianパッケージに関する変更点のみを記載します。
新規にパッケージングする場合、バージョン番号のあとに`-1`をつけます。
この`-1`というのがDebianでのリリース番号です。この番号はDebian特有の修正が入るたびにインクリメントされます。

生成されたテンプレートは次のようになっているはずです。

```text:~/work/libfreeaptx/debian/changelog
libfreeaptx (0.1.1-1) UNRELEASED; urgency=medium

  * Initial release (Closes: #nnnn) <nnnn is the bug number of your ITP>

 -- Debian User <debian@example.jp>  Sat, 06 Nov 2021 21:52:21
```

`dch -r`を実行すると、リリースの部分が更新され、`UNRELEASED`から`unstable`に更新されます。


最終的に、次のような内容にします。`Closes:`のところは前の章で実施したITPの番号を記入します。

```text:~/work/libfreeaptx/debian/changelog
libfreeaptx (0.1.1-1) unstable; urgency=medium

  * Initial release (Closes: #995607)

 -- Debian User <debian@example.jp>  Sat, 06 Nov 2021 21:52:21
```

<section class="column">

<h4>なぜCloses:をdebian/changelogに書くのか</h4>

ITPのときに登録したバグはいつ閉じられるのでしょうか。
答えは、パッケージがDebianのアーカイブに取り込まれたときです。
このときに、自動的にバグを閉じる仕組みになっています。

もし、そのような仕組みがなかったら、個別にバグを閉じたりしないといけません。
このように、自動化できるところは自動化されています。
</section>

### debian/controlを修正する

*debian/control*は生成されるパッケージを定義するためのファイルです。

生成されたテンプレートは次のようになっているはずです。

```text:~/work/libfreeaptx/debian/control
Source: libfreeaptx
Priority: optional
Maintainer: Debian User <debian@example.jp>
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.6.2
Section: libs
Homepage: <insert the upstream URL, if relevant>
#Vcs-Browser: https://salsa.debian.org/debian/libfreeaptx
#Vcs-Git: https://salsa.debian.org/debian/libfreeaptx.git
Rules-Requires-Root: no

Package: libfreeaptx-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libfreeaptxBROKEN (= ${binary:Version}), ${misc:Depends}
Description: <insert up to 60 chars description>
 <insert long description, indented with spaces>

Package: libfreeaptxBROKEN
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: <insert up to 60 chars description>
 <insert long description, indented with spaces>
```

`Source:`がlibfreeaptxとなっているので、libfreeaptxというソースパッケージから各種バイナリパッケージを作成することがわかります。
`Package:` というのがバイナリパッケージを示します。そのため、この記述のままだと`libfreeaptx-dev`と`libfreeaptxBROKEN`パッケージを生成するということがわかります。

次の点を修正しましょう。

* `Homepage:`が空なので記入する
* `Vcs-Browser:`と`Vcs-Git:`をアンコメントする
* `Depends:`は`libfreeaptx0`にする
* `Description:`のパッケージの詳細説明を更新する

例えば、`Package:`以降を次のように書き換えます。

```text:~/work/libfreeaptx/debian/control
Package: libfreeaptx-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libfreeaptx0 (= ${binary:Version}),
 ${misc:Depends}
Description: Development files for Free implementation of aptX
 libfreeaptx is based on version 0.2.0 of libopenaptx with the intent of continuing
 under a free license without the additional license restriction added to 
 libopenaptx 0.2.1.
 .
 This package contains development files for libfreeaptx.

Package: libfreeaptx0
Architecture: any
Multi-Arch: same
Depends:
 ${shlibs:Depends},
 ${misc:Depends}
Description: Free implementation of aptX
 libfreeaptx is based on version 0.2.0 of libopenaptx with the intent of continuing
 under a free license without the additional license restriction added to 
 libopenaptx 0.2.1.
 .
 The initial version of libfreeaptx was reset to 0.1.0 to prevent confusion between
 the two projects.
```

`Description:`は、サマリと詳細部分から構成されています。
最初の一行がサマリで、その後の部分が詳細説明です。
詳細説明に改行を入れる場合は、`.`を使います。

`Depends:`で`libfreeaptxBROKEN`をなぜ`libfreeaptx0`にすべきかというと、
生成されるライブラリーの.soが`libfreeaptx.so.0.1.1`であることに由来します。
これで、`libfreeaptx0`というライブラリーと、開発パッケージ`libfreeaptx-dev`というパッケージの2つを定義できました。


また、libfreeaptxでは2つのコマンドを提供しています。

* `freeaptxenc`
* `freeaptxdec`

これらのコマンドは実行バイナリーなので、`libfreeaptx0`や`libfreeaptx-dev`に含めるには不適切です。
そこで、これらのコマンドを提供する`freeaptx-utils`パッケージを作成することにします。
*debian/control*に追加でフィールドを足しましょう。

```text:~/work/libfreeaptx/debian/control
Package: freeaptx-utils
Architecture: any
Multi-Arch: no
Section: utils
Depends:
 ${shlibs:Depends},
 ${misc:Depends}
Description: Utility tools for Free implementation of aptX
 libfreeaptx is based on version 0.2.0 of libopenaptx with the intent of continuing
 under a free license without the additional license restriction added to
 libopenaptx 0.2.1.
 .
 This package contains utility tools such as freeaptxdec and freeaptxenc.
```

これで、*debian/control*で適切なパッケージを定義できました。

### debian/copyrightを修正する

*debian/copyright*は、ソフトウェアのライセンスを明記するためのファイルです。

`Files:`で対象ファイルを指定し、`Copyright:`で著作者を記載、`License:`でそのライセンスを明記します。

Copyrightの書き方については、[Machine-readable debian/copyright file](https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/) <span class="footnote">https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/</span> というドキュメントがあります。
詳細はそちらのドキュメントを参照してください。

ライセンスを明記すると*debian/copyright*は次のようになります。

```text:~/work/libfreeaptx/debian/copyright
Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: libfreeaptx
Upstream-Contact: Hunter Wardlaw <wardlawhunter@gmail.com>
Source: https://github.com/iamthehorker/libfreeaptx

Files: *
Copyright: 2017 Aurelien Jacobs <aurel@gnuage.org>
 2018-2020  Pali Rohár <pali.rohar@gmail.com>
 2021 Hunter Wardlaw <wardlawhunter@gmail.com>
License: LGPL-2.1+

Files: debian/*
Copyright: 2021 Kentaro Hayashi <kenhys@xdump.org>
License: GPL-2+

License: LGPL-2.1+
 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU Lesser General Public License as published by the Free Software
 Foundation; either version 2.1, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
 A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU Lesser General Public License
 version 2.1 can be found in "/usr/share/common-licenses/LGPL-2.1".

License: GPL-2+
 This package is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; either version 2 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General Public License version
 2 can be found in "/usr/share/common-licenses/GPL-2".
```

基本は対象ファイル(`Files:`)、著作権者(`Copyright:`)、適用するライセンス(`License:`)のフィールドがセットになっています。
適用するライセンスの詳細は、慣習として独立した箇所に`License:`フィールドを用いてその詳細を明記します。

### debian/freeaptxenc.mdを修正する

アップストリームがmanページを用意していない場合、メンテナーが用意するというのを推奨しています。
help2manなどから生成できますが、コマンドを提供していなかったりするとその方法は使えません。
ヘルプメッセージがあまり親切でなかったりすることもあるので、そういう場合はMarkdownから変換するなどするのがよいでしょう。

サンプルとして、`manpage.md.ex`を修正し、コマンドごとに.mdを作成する例を紹介します。
インストールされるコマンドが`freeaptxenc`なら、*freeaptxenc.md*とします。

```text:freeaptxenc.mdの内容(抜粋)
% freeaptxenc(1) | User Commands
%
% "October 15 2021"

# NAME

freeaptxenc - aptX encoder utility

# SYNOPSIS

**libfreeaptx** **-e** _this_ [**\-\-example=that**] [{**-e** | **\-\-example**} _this_]
  [{**-e** | **\-\-example**} {_this_ | _that_}]
**libfreeaptx** [{**-h** | *\-\-help**} | {**-v** | **\-\-version**}]

# DESCRIPTION

This utility encodes a raw 24 bit signed stereo
samples from stdin to aptX or aptX HD on stdout
...
```

記述したMarkdownは`pandoc`を使うと、次のようにしてmanページに変換できます。

```
$ pandoc --standalone --from=markdown --to=man freeaptxenc.md --output=freeaptxenc.1 
```
### libfreeaptx-dev.dirsを修正する

*libfreeaptx-dev.dirs*はインストール先のディレクトリが作成されるようにするためのものです。
開発用のライブラリーのファイルをインストールできるようにします。雛形そのままで問題ありません。

```
usr/lib
usr/include
```

### libfreeaptx-dev.installを修正する

*libfreeaptx-dev.install*は開発パッケージに含めるものを明示します。

`usr/share/pkgconfig/*`以下に.pcはインストールされないので、テンプレートからは削除します。
次のような内容にします。


```
usr/include/*
usr/lib/*/lib*.so
usr/lib/*/pkgconfig/*
```

### libfreeaptx0.dirsを修正する

*libfreeaptx0.dirs*はインストール先のディレクトリが作成されるようにするものです。
ランタイムのライブラリーのファイルをインストールできるようにします。雛形そのままで問題ありません。

```
usr/lib
```

### libfreeaptx0.installを修正する

*libfreeaptx0.install*は`libfreeaptx0`に含めるものを明示します。
テンプレートそのままで問題ありません。


```
usr/lib/*/lib*.so.*
```

### debian/rulesを修正する

*debian/rules*はパッケージをビルドするためのルールを定義しているファイルです。
`Makefile`の役割と同じです。

通常はテンプレートそのままで問題ないことも多いです。
しかし、`libfreeaptx`はAutotoolsのようなビルドツールに対応していないので、期待したパスにインストールできません。

そこで、インストールするときのルールを上書きしてあげる必要があります。

インストールするときに実行されるのは、`dh_auto_install`というルールなので、これを上書きします。
`dh_auto_install`というルールを上書きするには、`override_dh_auto_install`を定義します。

```
override_dh_auto_install:
       dh_auto_install -- PREFIX=/usr LIBDIR=lib/$(DEB_HOST_MULTIARCH)
```

`--` 以降はMakefileに渡される変数を指定します。

`libfreeaptx`の場合、Makefileが`PREFIX=/usr/local`になっていたので、`PREFIX=/usr`で上書きします。

また、`LIBDIR=lib`になっていたので、`LIBDIR=lib/$(DEB_HOST_MULTIARCH)`で上書きします。
`${DEB_HOST_MULTIARCH}`はビルド時にその環境の値へと置き換えられます。amd64なら`x86_64-linux-gnu`です。
したがって、*/usr/lib/x86_64-linux-gnu*以下にライブラリーがインストールされます。

### debian/salsa-ci.ymlを修正する

salsa.debian.orgではパッケージのCIを有効にできます。
テンプレートそのままで問題ありません。

### debian/watchを修正する


パッケージの更新をチェックするときのルールを定義するのが*debian/watch*です。
バージョン4が最新です。

GitHubのタグからバージョン情報を取得して、現在のパッケージより新しいものがあるかチェックするためのルールは次のようにします。


```
version=4
opts= filenamemangle=s/.+\/v?(\d\S+)@ARCHIVE_EXT@/@PACKAGE@-$1@ARCHIVE_EXT@/ \
  https://github.com/iamthehorker/@PACKAGE@/tags .*/v?(\d\S+)@ARCHIVE_EXT@
```

上記の例では、GitHubのtagsからバージョンに相当する文字列を抜き出し、(パッケージ名)-(バージョンの).tar.gzに変換する方法を指定しています。
HTMLを取得して文字列をマッチさせて変換しているため、GitHubのリリースページのアーカイブへのリンクのように、動的に生成されるページには向きません。

*debian/watch*をどう書くとよいかについては、*debian/watch*の解説wiki<span class="footnote">https://wiki.debian.org/debian/watch</span>があるのでそちらを参考にするのをおすすめします。

実際に新しいバージョンがあるかどうかは、`uscan`というコマンドを実行するとわかります。
終了コードが0ならば、新しいバージョンがあり、1なら新しいバージョンはありません。

*debian/watch*では複雑なスキャンルールを完結に記述するためにいくつか変数を利用できます。

|変数|説明|
|---|---|
|`@PACKAGE@`|*debian/changelog*のソースパッケージ名に置き換えられる|
|`@ANY_VERSION@`|バージョン表記にマッチする|
|`@ARCHIVE_EXT@`|拡張子にマッチする|
|`@SIGNATURE_EXT@`|署名されたファイルの拡張子にマッチする|
|`@DEB_EXT@`|debのrepack(+dfsgなど)にマッチする|

<section class="column">

<h4>パッケージに含められないファイルをどうするとよいか</h4>

パッケージングしたいソフトウェアによっては、ライセンスが不明瞭だったり、自由でないなどの
理由から、Debianパッケージには含めてはいけないものがupstreamで公開されている
アーカイブに含まれているということがありえます。

そのようなアーカイブがupstreamから公開されている場合には、
あらかじめ除去するという作業が必要です。

パッケージ名に+dsや+dfsgといったサフィックスがついているものをみたことがあるでしょうか。
+dsはdebianパッケージとしてインストールできる3rdパーティのライブラリーのソースコードが
アーカイブに含まれていたので除去した場合につけられます。

+dsfgはDebianのフリーソフトウェアガイドラインに準じていないため自由でなく配布に支障がある
ものを除去した場合につけられます。

具体的には、あらかじめ除去したアーカイブを作成し、再作成したアーカイブをもとにインポートして
パッケージングをします。手作業でそういった作業をするのは間違いやすいので、*debian/copyright*に`Files-Excluded:`を付与し、*debian/rules*で
再作成を指示する`repack`を指定したりすることで新しいバージョンをインポートする際に自動化できる用意されています。
</section>

## 修正内容をpushする

ここまでの変更結果をコミットすると、`gbp buildpackage`でパッケージがビルドできるようになります。

いったんここまでの成果をpushしておきましょう。
`upstream`と`pristine-tar`ブランチも忘れないで下さい。<span class="footnote">git remoteで指定した先にpushする権限があるものとして説明しています。そうでない場合は適宜読み替えてください。</span>
```
~/work/libfreeaptx $ git push origin debian/unstable
~/work/libfreeaptx $ git checkout upstream
~/work/libfreeaptx $ git push origin upstream
~/work/libfreeaptx $ git checkout pristine-tar
~/work/libfreeaptx $ git push origin pristine-tar
```

## 本章のまとめ

本章では、`libfreeaptx`を題材にして、ソフトウェアをパッケージングする具体的なやりかたを説明しました。

<section class="summary">

* リポジトリはsalsa.debian.orgに置くのがおすすめ
* `gbp orig-import`でアーカイブをインポートできる
* *debian/*の雛形は`dh_make`で作れる
* *debian/copyright*はしっかり書くべし
* `debian/unstable`だけでなく、`upstream`や`pristine-tar`ブランチもpushするのを忘れないこと

</section>

ひとまずパッケージらしきものはこれで作れるようになりましたが、よいパッケージングのお作法にならっているかというと、改善の余地があります。

次章では、よりよいパッケージングのやりかたついて説明します。

---

<br>

---
