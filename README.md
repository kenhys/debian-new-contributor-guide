# Debian New Contributor Guide

Debian新コントリビューターガイド

> created by [create-book](https://github.com/vivliostyle/create-book).

## ライセンス

全体のライセンスは [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.ja)とします。

## References

- VFM <https://vivliostyle.github.io/vfm/#/vfm>
- Vivliostyle CLI <https://github.com/vivliostyle/vivliostyle-cli#readme>
- Vivliostyle Themes <https://github.com/vivliostyle/themes#readme>
- Awesome Vivliostyle <https://github.com/vivliostyle/awesome-vivliostyle#readme>
- Vivliostyle (GitHub) <https://github.com/vivliostyle>
- Vivliostyle <https://vivliostyle.org>
