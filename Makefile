lint:
	npx textlint manuscripts/*.md

release:
	pdftk frontpage.pdf content-v1.3.0.pdf cat output debian-new-contributor-guide-v1.3.0.pdf

all:
	npm run build
	npm run press
