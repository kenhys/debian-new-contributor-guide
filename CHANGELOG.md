# 2024/11/2 - v1.3.0

第四版として頒布予定

* 第3章
  * nm.debian.orgの紹介を追記
* 第4章
  * mentors.debian.netが多言語化されていることを明記した。
* 第5章
  * reportbugの雛形に関する説明を追加した。
* 第6章
  * 対象となるバージョンをbullseyeからbookwormに改訂した。
  * よりホストにパッケージをインストールしなくてすむ手順に改訂した。
* 第7章
  * パッケージに含められないファイルの扱いについてコラムを追加した。
* 第8章
  * lintian.debian.org廃止にともない記述を改訂した。
  * lintian、autopkgtestがpbuilderのフックで実行される前提の手順に改訂した。
* 第9章
  * パッチに含めるべき情報について説明を追加した。
* 第10章
  * packaging-devは非推奨なので言及している箇所を削除した。
* 第13章
  * パッチがコンフリクトしたときの対処とフィードバックの重要性について説明した。
* golang編
  * packaging-devは非推奨なので言及している箇所を削除した。
* whalebuilder編
  * bookworm向けに改訂した。
* conoha編
  * bookworm向けに改訂した。
* debocker編
  * bookworm向けに改訂した。

# 2022/02/27 - v1.2.0

第三版として頒布済み。

## 修正

* 第2章 本書について p.7
  * 原文: 第5章から第12章までは、パッケージングのDebian公式アーカイブ入りを目指して作業する際に必要ことを実際のパッケージングを例に説明します。
  * 修正: 第5章から第12章までは、パッケージングのDebian公式アーカイブ入りを目指して作業する際に必要なことを実際のパッケージングを例に説明します。

* 第2章 本書について p.7
  * 原文: また、日本語で読める比較的新し目の情報のものでなければなりません
  * 修正: また、日本語で読める比較的新しめの情報のものでなければなりません

* 第3章 Debianプロジェクトを構成する人々 p.9
  * 原文: しかし、パッケージをメンテンナンスできます。
  * 修正: しかし、パッケージをメンテナンスできます。

* 第6章 パッケージング環境を構築してみよう p.28
  * 参考グラフの出典が https://trends.debian.net であることを明記した。

* 第7章 パッケージングしてみよう p.39
  * 原文: ~/work/libfeeaptx $ git init
  * 修正: ~/work/libfreeaptx $ git init

* 第7章 パッケージングしてみよう p.39
  * 原文: ~/work/libfeeaptx $ git remote add origin git@salsa.debian.org:debian/libfreeaptx.git
  * 修正: ~/work/libfreeaptx $ git remote add origin git@salsa.debian.org:debian/libfreeaptx.git

* 第7章 パッケージングしてみよう p.45
  * 原文: `Source:`がlibfeeaptxとなっているので、
  * 修正: `Source:`がlibfreeaptxとなっているので、

* 第8章 よりよいDebianパッケージにしてみよう p.65
  * 原文: 例えば、*libfreeaptx/debian/encode*を用意して、freeaptxencの実行結果をテストしたり、*libfreeaptx/debian/decode*を用意して、freeaptxdecの実行結果をテストしたりします。
  * 修正: 例えば、*libfreeaptx/debian/tests/encode*を用意して、freeaptxencの実行結果をテストしたり、*libfreeaptx/debian/tests/decode*を用意して、freeaptxdecの実行結果をテストしたりします。

* 第9章 パッケージの問題をフィードバックしよう p.67
  * 原文: `reporbug`を使ったバグ報告の流れは次のとおりです。
  * 修正: `reportbug`を使ったバグ報告の流れは次のとおりです。

* 第10章 パッケージをアップロードしてみよう p.75
  * 原文: たとえば、個別ページのQA informationの欄に次のように表示されます。このがグリーンな状態を目指しましょう。
  * 修正: たとえば、個別ページのQA informationの欄に次のように表示されます。これがグリーンな状態を目指しましょう。

* 第14章 さらなるヒントをもとめて p.90
  * 原文: https://www.debian.org/doc/debian-policy/upgrading-checklist.html
  * 修正: アップグレードチェックリスト (URL:https://www.debian.org/doc/debian-policy/upgrading-checklist.html を注釈で記載するように変更)

* 第14章 さらなるヒントをもとめて p.91
  * 原文: 実践編でも紹介ている、DebianプロジェクトでのGoのパッケージング方法について説明した記事です。
  * 修正: 実践編でも紹介している、DebianプロジェクトでのGoのパッケージング方法について説明した記事です。

* 実践: ConoHaで開発環境を構築する p.95
  * 原文: ConoHaならイメージ保存という手がので使えるのでコストをおさえることも可能
  * 修正: ConoHaならイメージ保存という手が使えるのでコストをおさえることも可能

* 実践: whalebuilderを使ってビルドする方法 p.109
  * 原文: `$GBP_CHANGES_FILE`はこの場合実際には存在しないので、ベース名をから正しい*.changes*のパスを生成して指定します。
  * 修正: `$GBP_CHANGES_FILE`はこの場合実際には存在しないので、ベース名から正しい*.changes*のパスを生成して指定します。
