module.exports = {
  //title: 'debian-new-contributor-guide', // populated into `publication.json`, default to `title` of the first entry or `name` in `package.json`.
  title: 'Debian新コントリビューターガイド', // populated into `publication.json`, default to `title` of the first entry or `name` in `package.json`.
  author: 'Kentaro Hayashi <kenhys@gmail.com>', // default to `author` in `package.json` or undefined.
  // language: 'ja', // default to undefined.
  // size: 'A4', // paper size.
  size: 'JIS-B5', // paper size.
  theme: 'theme-dncg/dncg.content.screen.css', // .css or local dir or npm package. default to undefined.
  entry: [
    //'manuscript.md', // `title` is automatically guessed from the file (frontmatter > first heading).
    // {
    //   path: 'epigraph.md',
    //   title: 'Epigraph', // title can be overwritten (entry > file),
    //   theme: '@vivliostyle/theme-whatever', // theme can be set individually. default to the root `theme`.
    // },
    { rel: 'contents', theme: 'theme-dncg/dncg.toc.screen.css' },
    'manuscripts/01introduction.md',
    'manuscripts/02scope.md',
    'manuscripts/03debian.md',
    'manuscripts/04flow.md',
    'manuscripts/05itp.md',
    'manuscripts/06setup.md',
    'manuscripts/07packaging.md',
    'manuscripts/08tweak.md',
    'manuscripts/09patch.md',
    'manuscripts/10mentors.md',
    'manuscripts/11rfs.md',
    'manuscripts/12reject.md',
    'manuscripts/13upgrade.md',
    'manuscripts/14faq.md',
    'manuscripts/conoha.md',
    'manuscripts/golang.md',
    'manuscripts/whalebuilder.md',
    'manuscripts/debocker.md',
    'manuscripts/lintian.md',
    'manuscripts/screen.md',
    'manuscripts/colophon.md'
  ], // `entry` can be `string` or `object` if there's only single markdown file.
  // entryContext: './manuscripts', // default to '.' (relative to `vivliostyle.config.js`).
  // output: [ // path to generate draft file(s). default to '{title}.pdf'
  //   './output.pdf', // the output format will be inferred from the name.
  //   {
  //     path: './book',
  //     format: 'webpub',
  //   },
  // ],
  // workspaceDir: '.vivliostyle', // directory which is saved intermediate files.
  toc: {
    title: '目次'
  },
  // cover: './cover.png', // cover image. default to undefined.
  vfm: { // options of VFM processor
    hardLineBreaks: false, // converts line breaks of VFM to <br> tags. default to 'false'.
    //disableFormatHtml: true, // disables HTML formatting. default to 'false'.
  },
}
